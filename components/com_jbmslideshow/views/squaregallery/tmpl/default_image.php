<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2012 Jan B Mwesigwa, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$thumbUrl = $this->helper->getThumbUrl(
                $this->image->get('data.image.filepath'), 
                array($this->params->get('width',150), $this->params->get('height',150)),
                null, 
                array('crop'=>$this->image->get('data.image.crop'))
            );
$linkUrl = $this->helper->getThumbUrl(
                $this->image->get('data.image.filepath'), 
                array(1000,1000)
            );
?>

<div class="plugin-image">
    <a class="preview" href="<?php echo $linkUrl; ?>" title="<?php echo $this->image->get('data.image.description.text',''); ?>">
        <div class="preview-icon"></div>
        <img src="<?php echo $thumbUrl; ?>" alt="<?php echo $this->image->get('data.image.description.text',''); ?>" />
    </a>
</div>