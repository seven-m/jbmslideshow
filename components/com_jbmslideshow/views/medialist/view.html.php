<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * HTML View class for the Media component
 *
 * @package		Joomla.Administrator
 * @subpackage	com_media
 * @since 1.0
 */
class JbmslideshowViewMediaList extends JView
{
	function display($tpl = null)
	{
        // Access check
        $user = JFactory::getUser();
        $asset = JRequest::getCmd('asset');
        $author = JRequest::getCmd('author');

        if (	!$user->authorise('core.manage', 'com_jbmslideshow')
            &&	(!$asset or (
                    !$user->authorise('core.edit', $asset)
                &&	!$user->authorise('core.create', $asset)
                && 	count($user->getAuthorisedCategories($asset, 'core.create')) == 0)
                &&	!($user->id==$author && $user->authorise('core.edit.own', $asset))))
        {
            return JError::raiseWarning(403, JText::_('JERROR_ALERTNOAUTHOR'));
        }

        $params = JComponentHelper::getParams('com_media');

        // Load the admin HTML view
        require_once JPATH_COMPONENT.'/helpers/media.php';
		
        // Do not allow cache
		JResponse::allowCache(false);

		$app	= JFactory::getApplication();
		$style = $app->getUserStateFromRequest('media.list.layout', 'layout', 'thumbs', 'word');

		$lang	= JFactory::getLanguage(); 
        
		JHtml::_('behavior.framework', true);
        JHTML::_('behavior.modal');

		$document = JFactory::getDocument(); 
		$document->addStyleSheet('components/com_jbmslideshow/css/medialist-'.$style.'.css'); 

        $images = $this->get('images');
		$documents = $this->get('documents');
		$folders = $this->get('folders');
		$state = $this->get('state');
        
		$document->addScriptDeclaration("
		window.addEvent('domready', function() {
			$$('a.img-preview').each(function(el) {
				el.addEvent('click', function(e) {
					new Event(e).stop();
					SqueezeBox.fromElement(el);
				});
			});
            window.parent.JbmslideshowMediaManager.init('$state->folder');
		});");

		$this->assign('baseURL', JURI::root());
		$this->assignRef('images', $images);
		$this->assignRef('documents', $documents);
		$this->assignRef('folders', $folders);
		$this->assignRef('state', $state);

        $this->setLayout('details');
        
		parent::display($tpl);
	}

	function setFolder($index = 0)
	{
		if (isset($this->folders[$index])) {
			$this->_tmp_folder = &$this->folders[$index];
		} else {
			$this->_tmp_folder = new JObject;
		}
	}

	function setImage($index = 0)
	{
		if (isset($this->images[$index])) {
			$this->_tmp_img = &$this->images[$index];
		} else {
			$this->_tmp_img = new JObject;
		}
	}

	function setDoc($index = 0)
	{
		if (isset($this->documents[$index])) {
			$this->_tmp_doc = &$this->documents[$index];
		} else {
			$this->_tmp_doc = new JObject;
		}
	}
}
