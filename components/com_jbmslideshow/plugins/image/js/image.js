// Image Plugin API
var jcrop_api,
    ImagePlugin = {
    init : function() {
    },
    label : "image",
    onClick : function($slide) { ImagePlugin._changeImage.call(this, $slide) },
    controls : {
        changeImage : {
            label: 'change image', 
            fn: function($slide) { ImagePlugin._changeImage.call(this, $slide) }
        },
        crop : {
            create: false,
            label: 'crop', 
            fn: function($slide) { ImagePlugin._crop.call(this, $slide) }
        },
        changeDescription : {
            label: 'description',
            fn: function($slide, data) { ImagePlugin._changeDescription.call(this, $slide) }
        }
    },
    _changeDescription : function($slide) {
        var manager = this,
            data = manager.getSlideData($slide),
            $content = $slide.clone()
                        .append(manager.$('<span class="label">Description:</span><input type="text" class="field-description"/>'))
                        .append(manager.$('<br />'))
                        .append(manager.$('<span class="label">Font color:</span><select class="field-color" /><span class="label">Background color:</span><select class="field-background" /><span class="label">Font size:</span><select class="field-font-size" /><br />'))
                        .append(manager.$('<span class="label">Position:</span><span class="label">top</span><input type="radio" name="field-position" value="top"/><span class="label">bottom</span><input type="radio" name="field-position" value="bottom"/>')); 
        
        // get values or fill with defaults
        var desc = data.description ? data.description : {};
        desc = manager.$.extend({}, {fontSize: '100%', background: 'default', color: '', text: '', position: 'bottom'}, desc);

        // adjust image and prepare controls
        $content.find('.field-description').val(desc.text);
        $content.find('img').css({width: manager.get('initial_width', 600, 'int'), height: 'auto'});
        $content.find('.field-description').change(function() {$content.find('.description').text(manager.$(this).val())});
        manager.$.each(['default','white','black','red','yellow','green','blue'], function() { 
            $content.find('select.field-color').append(manager.$('<option/>').text(this).attr('value',this)); 
            $content.find('select.field-background').append(manager.$('<option/>').text(this).attr('value',this)); 
        }); 
        manager.$.each(['100%','130%','160%','180%','250%'], function() { 
            $content.find('select.field-font-size').append(manager.$('<option/>').text(this).attr('value',this)); 
        });
        
        $content.find('.field-color')
            .change(function(){ 
                if (manager.$(this).val() == 'default') {
                    $content.find('.description').css('color', '');
                } else {
                    $content.find('.description').css('color', manager.$(this).val());
                }
            })
            .val(desc.color).change();
            
        $content.find('.field-background')
            .change(function(){ 
                if (manager.$(this).val() == 'default') {
                    $content.find('.background').css('background-color', '');
                } else {
                    $content.find('.background').css('background-color', manager.$(this).val());
                }
            })
            .val(desc.background).change();
            
        $content.find('.field-font-size')
            .change(function(){
                $content.find('.description').css('font-size', manager.$(this).val());
            })
            .val(desc.fontSize).change();
            
        $content.find('input:radio[name="field-position"]')
            .change(function(){
                if (manager.$(this).val() == 'top') {
                    $content.find('.description-wrapper').css({'top': 0, 'bottom': 'auto'});
                } else {
                    $content.find('.description-wrapper').css({'bottom': 0, 'top': 'auto'});
                }
            })
            .filter('[value='+desc.position+']').attr('checked', true)
            .change();

        // push all into system dialog
        manager.dialog.open({
            label: "Set image description",
            width: manager.get('initial_width', 600,'int') + 40,
            height: manager.get('initial_height', 400,'int') + 230,
            buttons: {
                save : function(manager) { ImagePlugin._saveDescription.call(manager, $slide); manager.dialog.close(); },
                close : function(manager) { manager.dialog.close(); }
            },
            content: $content
        });
        
        // some additional controls setup
        $content.find('.description-wrapper').css('width', $content.find('img').width());
    },
    _saveDescription : function($slide) {
        var manager = this,
            desc;
        
        // collect values
        desc = {
            fontSize : manager.$('.field-font-size', manager.dialog.$dialog).val(),
            background : manager.$('.field-background', manager.dialog.$dialog).val(),
            color : manager.$('.field-color', manager.dialog.$dialog).val(),
            text : manager.$('.field-description', manager.dialog.$dialog).val(),
            position : manager.$('input:radio[name="field-position"]:checked', manager.dialog.$dialog).val(),
        }
        manager.updateSlide($slide, { description: desc });
    },
    _changeImage : function($slide) {
        var manager = this;
        manager.browse({
            label: 'Select an image',
            plugin: 'image', 
            clickfn: function($img, data){
                var $c = $img
                            .clone()
                            .css({ 
                                width: manager.get('preview_width',200,'int')
                            });
                            
                // add crop button
                if (!manager.buttonExists($slide, ImagePlugin.controls.crop.label)) {
                    manager.addControls($slide, ImagePlugin.controls.crop );
                }
                data.size = [];
                data.size.push(manager.get('width',600, 'int'));
                data.size.push(manager.get('height',400, 'int'));
                data.crop = null;
                manager.updateSlide($slide, data);
            } 
        });
    },
    _crop : function($slide) {
        var manager = this,
            data = manager.getSlideData($slide),
            $img = $slide.find('img'),
            $cropimage = manager.$('<img />').attr('src', data.url); 
        
        // open in edit window
        manager.dialog.open({
            label : "Crop the image",
            width: 420,
            height: 420,
            overflow: false,
            buttons : {
                    save : function(manager) { ImagePlugin._save.call(manager, $slide); manager.dialog.close(); },
                    close : function(manager) { jcrop_api.destroy(); manager.dialog.close(); }
                },
            content : $cropimage
        });
        // start JCrop
        // TODO: minSize, aspectRatio
        $cropimage.Jcrop({
                boxHeight: 400,
                boxWidth: 400,
                aspectRatio: manager.get('width',600,'int') / manager.get('height',400,'int'),
                minSize: [50, 50],
                handleSize: 3,
                onSelect : function() {
                    var selection = jcrop_api.tellSelect(),
                        scaleX, scaleY, scale;
                    
                    if (!selection.w || !selection.h)
                        return false;

                    if (selection.w > selection.h) {
                        
                    } else {
                        
                    }
                }
            }, 
            function(){
                jcrop_api = this;
                // set selection
                var data = manager.getSlideData('current'),
                    crop = data.crop ? data.crop : [0.2,0.2,0.3,0.3],
                    w = $cropimage.width(), 
                    h = $cropimage.height();
                if (crop) {
                    jcrop_api.setSelect([   
                        crop[0]*w, 
                        crop[2]*h, 
                        w - (crop[1]*w),
                        h - (crop[3]*h)
                    ]);
                } else {
                    jcrop_api.setSelect([ 0, 0, $cropimage.width(), $cropimage.height() ]);
                }
            }
        );
    },
    _save : function($slide) {
        var manager = this,
            data = manager.getSlideData($slide),
            args={}, params=[], crop = [], size = [],
            l, t, r, b, w, h,
            selection = jcrop_api.tellSelect(),
            bounds = jcrop_api.getBounds();
        
        for(var key in selection){
            params.push(selection[key]);
        }
        for(var key in bounds){
            params.push(bounds[key]);
        }

        w = params[6];
        h = params[7];
        l = params[0]/w;
        t = params[1]/h;
        r = (w - params[2])/w;
        b = (h - params[3])/h;
        
        crop = [l,r,t,b];
        for (var i = 0; i < crop.length; i++) {
            crop[i] = parseFloat(crop[i]).toFixed(3);
        }
        size = [manager.get('width', 500,'int'),manager.get('height', 350, 'int')];
        
        manager.updateSlide($slide, { crop: crop, size: size });
        
        jcrop_api.destroy();
    },
    /*  public method called from slideshow 
    *   'this' refers to slideshow methods.
    */
    update : function($slide, data, manager) {
        var slideshow = this,
            src, uri, size=[], desc='';
        
        uri = { 
            src : data.path ? data.path : '',
            fltr : []
        };

        if (data.crop) uri.fltr.push('crop|' + data.crop.join('|'));
        uri.fltr.push('size|' + [slideshow.get('width', 600, 'int'), slideshow.get('height', 400, 'int')].join('|') + '|0');

        src = slideshow.get('pluginUrl') + '/libraries/phpThumb/phpThumb.php?' + slideshow.$.param(uri);
        width = slideshow.get('width', 600, 'int');
        
        desc = data.description ? data.description : {fontSize: '100%', background: 'default', color: '', text: '', position: 'bottom'};

        if ($slide.html() == '') {
            manager.getLayout('image', function($layout) {
                $layout.find('img')
                        .css({
                            width: width
                        })
                        .attr('src', src)
                    .end().find('.description')
                        .text(desc.text)
                        .css({'color': desc.color, 'font-size': desc.fontSize })
                    .end().find('.description-wrapper')
                        .css({'top': desc.position == 'top' ? 0 : 'initial', 'bottom': desc.position == 'bottom' ? 0 : 'initial'})
                    .end().find('.background')
                        .css({'background-color': desc.background == 'default' ? '' : desc.background });
                $slide.empty().append($layout);
            });
        } else {
            $slide.find('img')
                    .css({
                        width: width
                    })
                    .attr('src', src)
                .end()
                .find('.description')
                    .text(desc.text)
                    .css({'color': desc.color, 'font-size': desc.fontSize })
                .end().find('.description-wrapper')
                    .css({'top': desc.position == 'top' ? 0 : 'initial', 'bottom': desc.position == 'bottom' ? 0 : 'initial'})
                .end().find('.background')
                    .css({'background-color': desc.background == 'default' ? '' : desc.background});
        } 
    }

} // end of var ImagePlugin
var JbmslideshowPlugin = {};
JbmslideshowPlugin['image'] = ImagePlugin;