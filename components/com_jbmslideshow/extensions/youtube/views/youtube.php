<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2012 Jan B Mwesigwa, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$item = isset($this->item) ? $this->item : new JRegistry();
?>

<div class="plugin-youtube">
    <div class="wrapper">
        <img src="<?php echo $item->get('data.thumbimage', COM_JBMSLIDESHOW_URL.DS.'images'.DS.'blank.gif'); ?>" alt="<?php echo $item->get('data.title', ''); ?>"/>
    </div>
</div>