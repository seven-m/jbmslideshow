<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2012 Jan B Mwesigwa, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$first = new JRegistry; 
$first->loadArray($this->content[0]);
$iframeSrc = JURI::getInstance("https://www.youtube.com/embed/".$first->get('data.youtube.videoId',''));
$iframeSrc->setVar('wmode','transparent');
$iframeSrc->setVar('controls',$this->params->get('controls',0));
$iframeSrc->setVar('autoplay',$this->params->get('autoplay',1));
$iframeSrc->setVar('modestbranding',1);
$iframeSrc->setVar('showinfo',$this->params->get('showinfo',0));
$iframeSrc->setVar('if_load_policy',3);
$iframeSrc->setVar('rel', $this->params->get('rel',0));
$iframeSrc->setVar('loop', $this->params->get('loop',1));
?>

<div class="player">
    <span class="fullscreen-button">
        <?php echo JText::_('COM_JBMSLIDESHOW_VIEW_YOUTUBE_FULLSCREEN'); ?>
    </span>
    <iframe id="jbmslideshow_player" allowfullscreen="1" frameborder="0" src="<?php echo $iframeSrc->toString(); ?>"></iframe>
    <div class="title">
        <?php echo $first->get('data.youtube.title'); ?>
    </div>
    <div class="description">
        <?php echo $first->get('data.youtube.description'); ?>
    </div>
    <a href="<?php echo $first->get('data.link.url',''); ?>" class="readmore" style="display:<?php echo $first->get('data.link.url','') ? 'block' : 'none'; ?>">
        <?php echo JText::_('COM_JBMSLIDESHOW_VIEW_YOUTUBE_READMORE'); ?>
    </a>
</div>
<div class="slides">
    <?php
        $perPage = (int) $this->params->get('slidesPerPage',8);
        $page = 1;
        $i = 0;
        $isLast = false;
        echo '<div class="page" rel="page'.$page.'">';
        foreach ($this->content as $k => $item) {
            if ($item['extension'] === 'youtube') { 
                $isLast = $k+1 == count($this->content);
                $this->displaySlide($item);
                $i++;
                if ($i == $perPage * $page) { 
                    $page++; 
                    echo '</div>';
                    if (!$isLast) echo '<div class="page" rel="page'.$page.'">';
                }
            }
        }
        echo $isLast ? '' : '</div>';
    ?>
</div>