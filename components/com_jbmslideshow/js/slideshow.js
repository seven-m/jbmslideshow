(function ($, window, document) {
	$(function () {
        
        var log = {
            debug : true,
            error : function(message) {
                var m = 'Jbmslideshow error';
                try {
                  if (this.debug && window.console && window.console.error){
                    window.console.error([m, message].join(': '));
                  }
                } catch (e) {
                  // no console available
                }
            },
            info : function(message) {
                var m = 'Jbmslideshow notice';
                try {    
                    if (window.console && window.console.info){
                        window.console.info([m, message].join(': '));
                    }
                } catch (e) {
                  // no console available
                }
            }
        }
        
        var methods = {
            started : false,
            xhrPool : [],
            _request : function(task, args, handle) {
                return $.ajax({
                    url: 'index.php?option=com_jbmslideshow&view=slideshow',
                    type: 'post',
                    data: { task: task, args: args },
                    success: function(data) { if (handle) handle.call(methods, data) },
                    error: function(result) { log.error(result.responseText);  },
                    beforeSend: function(jqXHR) { methods.xhrPool.push(jqXHR) }
                });
            },
            init : function( options ) { 
                return this.each(function() {
                    var $this = $(this),
                        settings = {
                            autoplay : false,
                            speed : 500,
                            timeout : 4000,
                            effect : 'fade',
                            pause : true,
                            width : 600,
                            height : 400,
                            preview_width : 200,
                            contentSelector : '.jbmslideshow-content',
                            slideSelector : '.slide',
                            slideClass : 'slide'
                        };

                    // plugin not yet initialized?
                    if (typeof $this.data('jbmslideshow') == 'undefined') {   
                        $this.data('jbmslideshow', $.extend(settings, options));
                        methods.$this = $this;
                        o = methods.getSettings();
                        o.Itemid = methods.get('Itemid');
                        o.id = methods.get('id');
                        o.API = {
                            get : methods.get,
                            set : methods.set,
                            getSettings : methods.getSettings,
                            getData : methods.getData,
                            setData : methods.setData,
                            getExtensionData : methods.getExtensionData,
                            setExtensionData : methods.setExtensionData,
                            getSlideExtensionName : methods.getSlideExtensionName,
                            getThumbSrc : methods.getThumbSrc,
                            ExtensionManager : methods.ExtensionManager,
                            setSlideDefaultData : methods.setSlideDefaultData,
                            swipeBox : methods.swipeBox,
                            lang : methods.lang,
                            isMobile : ('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0)
                        }
                    } else {
                        log.info('already initialized');
                        return;
                    }
                    
                    // TODO check compatibility
                    // check JSON.stringify()
                    // check parseJSON

                    // disable user interaction if waiting for AJAX requests
                    $('body')
                        .append($('<div id="jbmslideshow-overlay" />').hide()
                            .append($('<div id="jbmslideshow-overlay-background" />'))
                            .append($('<div id="jbmslideshow-overlay-message"/>')
                                    .append($('<img src="' + o.pluginUrl + '/images/icon-loader-32.gif" />'))
                                    .append($('<a class="cancel-all-button" href="#">cancel</a>')
                                                .click(function(e){ 
                                                    e.preventDefault();
                                                    methods.xhrPool.abortAll();
                                                    location.href = location.href;
                                                    $('#jbmslideshow-overlay').fadeOut();
                                                })
                                            )
                            )
                        );
                    var ajaxTimer;
                    $('#jbmslideshow-overlay')
                        .ajaxStop(function(){ $(this).fadeOut(); window.clearTimeout(ajaxTimer); })
                        .ajaxStart(function(){ var $t = $(this); ajaxTimer = setTimeout(function() { $t.fadeIn() }, 2000); });
                    // support for canceling all ajax requests
                    methods.xhrPool = [];
                    methods.xhrPool.abortAll = function() {
                        $(this).each(function(idx, jqXHR) {
                            jqXHR.abort();
                        });
                        methods.xhrPool.length = 0
                        $('#overlay').fadeOut();
                    };
                    
                    // transfer data attribute into elements data
                    $('[data]', $this.find(o.contentSelector)).each(function(){
                        methods.setData($(this), $.parseJSON($(this).attr('data')));
                        //$(this).data('jbmslideshow', $.parseJSON($(this).attr('data')));
                        $(this).attr('data','');
                    });
                    
                    // check if we have css
                    var href = methods.get('pluginUrl') +'/css/jbmslideshow.css';
                    if (!$('link[href~="'+ href +'"]').length) {
                        var link = $('<link />');
                        $('head').append(link); //IE hack: append before setting href
                        link.attr({
                            rel:  "stylesheet",
                            type: "text/css",
                            href: href
                        });
                        log.info('main jbmslideshow CSS loaded.');
                    }
                    
                    log.info('initialized');
                    
                    // register extensions
                    $this.trigger('jbmslideshow.onAddExtensions');
                    $.when.apply($, methods.ExtensionManager.queue).done(function() {
                        log.info('All extensions loaded');
                        // run extensions init methods
                        $.each(methods.ExtensionManager.getExtensions(), function(i, ext) {
                            if (ext.onInit) ext.onInit.call(ext, $(methods.get('slideSelector'), $this) );
                        });
                        methods._ready();
                    }); 
                    
                });
            },
            /*  The ready function gets executed on every 
            *   instance of the player after all extensions are registered. 
            */
            _ready : function() {
                var $this = methods.$this,
                    o = methods.getSettings();
                
                $this.trigger('jbmslideshow.onReady');
                log.info('ready');
                methods._start();
            },   
            _start : function() {
                log.info('start');
                var $this = methods.$this,
                    o = methods.getSettings(),
                    $slides = $(o.slideSelector, $this.find(o.contentSelector));

                // recheck if data attribute is transferred into elements data
                $('[data]', $this.find(o.contentSelector)).each(function(){
                    if (!methods.getData(this)) {
                        methods.setData(this, $.parseJSON($(this).attr('data')));
                        $(this).attr('data','');
                    }
                });
                
                // do we have anything to show?
                if ($slides.length < 1) { 
                    log.info('There are no slides to show');
                    return;
                } 
                    
                // update slides from extensions
                // changed: not needed anymore
                /*
                $slides.each(function(){
                    var ext = methods.getSlideExtensionName(this),
                        $slide = $(this),
                        data = methods.getExtensionData(this, ext);
                    
                    if (data) {
                        methods.ExtensionManager.trigger(ext, 'update', [$slide, data]);  
                    }
                });
                */
                
                // execute all onStarted methods
                methods.started = true;
                if (!o.logged) {
                    $this.trigger('jbmslideshow.onStarted', methods.getAPI());
                }
            },
            getSlideExtensionName : function(slide) {
                var $this = methods.$this,
                    $slide = $(slide);
                    API = methods.getAPI();
                return (API.getData($slide) && API.getData($slide).extension) ? API.getData($slide).extension : null;
            },
            getData : function(el, slide) {
                var result = new Array(), $el=$(el);
                
                // check if we have a collection
                if ($el.length > 1) {
                    $el.each(function(){
                        var $el = $(this);
                        // check if it's inside of a slide
                        if (slide && $(this).parents(API.get('slideSelector')).length) {
                            $el = $(this).parents(API.get('slideSelector'));
                        }
                        result.push(typeof $el.data('jbmslideshow') == 'undefined' ? null : $el.data('jbmslideshow'));
                    });
                    return result;
                } else {
                    // check if it's inside of slide
                    if (slide && $(el).parents(API.get('slideSelector')).length) {
                        $el = $(el).parents(API.get('slideSelector'));
                    }
                    return typeof $el.data('jbmslideshow') == 'undefined' ? null : $el.data('jbmslideshow');
                }
            },
            setData : function(el, data, slide){
                var $el = $(el);
                
                $el.each(function(){
                    var $el = $(this);
                    // check if it's inside of slide
                    if (slide && $(el).parents(API.get('slideSelector')).length) {
                        $el = $(el).parents(API.get('slideSelector'));
                    }
                    if (!$el.data('jbmslideshow')) $el.data('jbmslideshow', {});
                    return $el.data('jbmslideshow', $.isPlainObject(data) ? data : {});
                });
            },
            // returns reference to slide's extension data
            getExtensionData : function(el, ext, key) {
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings(),
                    $el = $(el),
                    ext = typeof ext == 'string' ? ext : ext.name ? ext.name : null;
                
                // check if it's registered extension
                if (!o.extensions[ext] || !o.extensions[ext].registered) return null;
                
                // passed el can be also .slide-content or anythig inside the slide 
                // then we need the data of the parent
                if (!$el.hasClass(o.slideClass) && $el.parents(o.slideSelector).length > 0) {
                    $el = $el.parents(o.slideSelector);
                }
                var data = $el.data('jbmslideshow').data ? $el.data('jbmslideshow').data[ext] : null;
                if (typeof key == 'string') {
                    return data && data[key] ? data[key] : null;
                } else 
                    return data;
            },
            getThumbSrc : function(params) {
                var API = methods.getAPI(),
                    filters = [];
                
                params = $.extend({
                    file: '',
                    transparency: false,
                    mimetype: 'jpg',
                    folder: 'images',
                    size: [100,100],
                    filters : {}
                }, params);
                
                params.f = params.transparency ? 'png' : 'jpg';
                params.src = API.get('mediaPath') + '/' + params.folder + '/' + params.file;
                params.size = $.isArray(params.size) ? 'size|' + [params.size[0], params.size[1],'0'].join('|') : 'size|' + [params.size[0], params.size[1],'0'].join('|');

                $.each(params.filters, function(name,values) {
                    if (!$.isArray(values)) return;
                    filters.push('fltr[]=' + name + '|' + values.join('|'));
                });
                return API.get('pluginUrl') + '/libraries/phpThumb/phpThumb.php?src=' + params.src + '&f=' + params.mimetype + '&fltr[]=' + params.size + '&' + filters.join('&'); 
            },
            setExtensionData : function(el, data, ext) {
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings(),
                    $els = $(el),
                    ext = typeof ext == 'string' ? ext : ext.name ? ext.name : null;

                if (!ext) return null;
                $els.each(function() {
                    $el = $(this);
                    
                    // passed el can be also .slide-content or anythig inside the slide 
                    // then we need the data of the parent
                    if (!$el.hasClass(o.slideClass) && $el.parents(o.slideSelector).length > 0) {
                        $el = $el.parents(o.slideSelector);
                    }
                    
                    if ($.isPlainObject(data)) {
                        var d = {}; d[ext] = data;
                        return $.extend($el.data('jbmslideshow').data, d)[ext];
                    } else return null;
                });
            },
            setSlideDefaultData : function(slide, ext) {
                $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings(),
                    $slide = $(slide),
                    data = {extension: ext.name, data:{}};
                
                data.data[ext.name] = {};

                if ($slide.parents(API.get('slideSelector')).length) $slide = $slide.parents(API.get('slideSelector'));
                
                return $slide.data('jbmslideshow', data);
            },
            get : function(param, def, type) {
                var $this = methods.$this,
                    o = methods.getSettings();
                type = type ? type : '';
                switch (type) {
                    case 'int' : 
                        return o[param] ? parseInt(o[param]) : (def ? def : false);
                        break;
                    case 'float' : 
                        return o[param] ? parseFloat(o[param]) : (def ? def : false);
                        break;
                    case 'boolean' :
                        if (typeof o[param] === 'boolean') {
                            return o[param];
                        } else if (typeof o[param] === 'string') {
                            p = o[param].toLowerCase(); 
                            return (p == 'true' || p == '1') ? true : false;
                        } else if (typeof o[param] === 'number') {
                            return o[param] == 0 ? false : true;
                        } else {
                            return (def ? def : false);
                        }
                        break;
                    default : return o[param] ? o[param] : (def ? def : '');
                }
            },
            set : function(param, value) {
                var $this = methods.$this,
                    o = methods.getSettings();

                if (!o) return false;
                o[param] = value;
                return true;
            },
            getSettings : function() {
                var $this = methods.$this;
                return $this.data('jbmslideshow');
            },
            getAPI : function() {
                var $this = methods.$this,
                    o = methods.getSettings();
                return o.API;
            },
            swipeBox : function(el) {
                var API = methods.getAPI(),
                    $el = $(el);
                if (!$.fn.swipebox) {
                    $.ajax({
                        url : API.get('pluginUrl') + '/libraries/swipebox/source/jquery.swipebox.min.js',
                        type : 'GET',
                        cache : true
                    })    
                    .done(function(data) {
                        // load main css
                        $('<link />')
                            .appendTo($('head'))
                            .attr({type: 'text/css', rel: 'stylesheet'})
                            .attr('href', API.get('pluginUrl') + '/libraries/swipebox/source/swipebox.css');
                        $el.swipebox();
                    })
                } else {
                    $el.swipebox();
                }
            },
            ExtensionManager : {
                queue : [],
                add : function(name, extObj) {
                    var $this = methods.$this,
                        o = methods.getSettings(),
                        extensions = o.extensions,
                        API = methods.getAPI();
                        ext = extensions[name];
                    
                    if (!this.verify(name)) {
                        log.info('extension "'+ name +'" is not valid.');
                        return;
                    }
                    
                    if (!this.isRegistered(name)) {
                        // register extension
                        $.extend(ext, extObj);
                        // check that a plugin has update method
                        if (ext.type == 'plugin' && typeof ext.update == 'undefined') { 
                            log.info('plugin "' + name + '" was not registered. "update" method missing.');
                            return;
                        }
                        ext.registered = true;
                        ext.getAPI = methods.getAPI;
                        ext.getView = function(params, callback) {
                            if (!$.isPlainObject(params) || typeof params['view'] == 'undefined') return false;
                            params = $.extend({
                                extension: name,
                                format: 'raw',
                            }, params);
                            methods._request('getExtensionView', params, function(result){
                                if (callback) callback.call(ext, result);
                            });
                        }
                        log.info('extension "' + name + '" successfully registered');
                    } else {
                        log.info('extension "' + name + '" is already registered');
                    }

                },
                getExtensions : function() {
                    var exts = methods.get('extensions'),
                        result = {};
                    $.each(exts, function(name, e) {
                        if (e.registered) result[name] = e;
                    });
                    return result;
                },
                isRegistered : function(name) {
                    var o = methods.getSettings(),
                        extensions = o.extensions;
                    return (typeof extensions[name] != 'undefined' && extensions[name].registered) ? true : false;
                },
                verify : function(name) {
                    var exts = methods.get('extensions');
                    return typeof exts[name] == 'undefined' ? false : true;
                },
                trigger : function(extName, method, args, callback) {
                    var extensions = methods.getSettings().extensions;
                    if (!$.isArray(args)) args = [args];
                    if (this.isRegistered(extName)) {
                        if(extensions[extName][method] ) {
                            c = extensions[extName][method].apply(extensions[extName], args);
                            if (callback && c) callback(c);
                        }
                    } else log.error('Extension "' + extName + '" does not exist or it has no "'+ method +'" method.');
                },
                triggerEvent : function(event, args) {
                    var exts = this.getExtensions();
                    $.each(exts, function() {
                        if (this[event]) this[event].apply(this, args);
                    });
                }
            },
            lang : function(key, def) {
                var API = methods.getAPI();
                return API.translations[key] ? API.translations[key] : def ? def : key;
            },
            /*
            * public method 
            * this refers to div container
            */
            addExtension : function(name, extObj) {
                if (typeof name == 'undefined' || typeof extObj == 'undefined') return;
                return this.each(function() {
                    var $this = $(this);
                    $this.bind('jbmslideshow.onAddExtensions', function(event) {
                        methods.ExtensionManager.add(name, extObj);
                    });
                })
            }
        }
        
        $.fn.jbmslideshow = function( method ) {
            var public = 'addExtension',
                isPublic = false;
            
            if (typeof method === 'string') {
                var r = new RegExp(method, 'i');
                isPublic = r.test(public);
            }
            
            // Method calling logic
            if (typeof method ==='string' && !isPublic) {
                log.error( method + ' is a protected method on jQuery.Jbmslideshow' );
            } else if ( methods[method] ) {
                return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply( this, arguments );
            } else {
                log.error( 'Method ' +  method + ' does not exist on jQuery.Jbmslideshow' );
            } 
        };
    });
} (this.jQuery, this, this.document));        