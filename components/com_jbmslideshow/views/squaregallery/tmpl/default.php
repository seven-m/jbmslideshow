<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2012 Jan B Mwesigwa, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
?>

<?php
    $content = array();
    $currCat = JRequest::getVar('category', count($this->content) ? $this->content[0]['data']['categories']['category'] : null);
    $cats = array();
    $itemData = new JRegistry();

    // filter category
    foreach ($this->content as $n => $item) {
        $itemData->loadArray($item);
        $cats[] = $itemData->get('data.categories.category');
        if ($itemData->get('data.categories.category') == $currCat) { 
            $content[] = $item;
        }
    }
    
    // display list of categories
    $cats = array_unique($cats);
    if (count($cats) > 1) {
        $catlist = '<div class="categorylist"><ul>';
        foreach($cats as $cat) {
            $catlist .= '<li><a href="'.JURI::current().'?category='.$cat.'" title="">'.$cat.'</a></li>';
        }
        echo $catlist .= '</ul></div><div class="row-separator clear"></div>';
    }
    
    // display slides
    $horiz = 10;
    $vert = 8; 
    $numPages = 1;
    $imageFrame = '<div class="image-frame">
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <img />
                                        <div class="description"></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>';
    $numImgs = 0;
    $row = 1;

    foreach ($content as $i => $item) {
        if (!preg_match('/(image|pagebreak)/i', $item['extension'])) continue;
        $isLast = $i+1 == count($content);
        $isFirst = $i == 0;
        
        if ($isFirst && $item['extension'] !== 'pagebreak' ) { 
            echo '<div class="page" rel="'.$numPages.'"><div class="slides">';
            continue;
        } elseif ($isFirst) {
            echo '<div class="page" rel="'.$numPages.'">';
            $this->displaySlide($item);
            echo '<div class="slides">';
            continue;
        }
        
        if ($item['extension'] === 'pagebreak') { 
            $numPages++;
            // add placeholders
            if ($numImgs <= ($horiz * $vert)-$numImgs) {
                $p = $horiz * $vert - $numImgs;
                while($p) {
                    echo '<div class="placeholder"></div>';
                    $numImgs++;
                    $p--;
                }
            }
            echo $imageFrame;
            echo '</div>';
            echo '</div>';
            echo '<div class="page" rel="'.$numPages.'">';
            $this->displaySlide($item);
            echo '<div class="slides">';
            $numImgs = 0;
            $row = 1;
        } else {
            // display image
            $this->displaySlide($item);
            $numImgs++;
            // add placeholders
            if ($isLast && $numImgs <= ($horiz * $vert)-$numImgs) {
                $p = $horiz * $vert - $numImgs;
                while($p) {
                    echo '<div class="placeholder"></div>';
                    $numImgs++;
                    $p--;
                }
            } elseif ($numImgs > $horiz && $numImgs == ($row * $horiz) + 1 && $row+1 < $vert) {
                $p = $horiz - 2;
                while($p) {
                    echo '<div class="placeholder"></div>';
                    $numImgs++;
                    $p--;
                }
                $row++;
            }
        }
        
        if ($isLast) {
            echo $imageFrame;
            echo '</div>';
            echo '</div>';
        }
    }
?>