<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
jimport('joomla.registry.registry');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');
require_once(COM_JBMSLIDESHOW_ADMIN.DS.'helpers'.DS.'jbmslideshow.php');
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 */
class JbmslideshowViewImageslist extends JView
{   
	public function display($tpl = null)
	{
		$app		= JFactory::getApplication();
        $actions    = JbmslideshowHelper::getActions();
        $helper     = new JbmslideshowHelper();
        
        if (!$actions->get('core.admin')) {
            echo "No access";
            return;
        }
        
        // get images 
        $list = JFolder::files(COM_JBMSLIDESHOW_IMAGES, 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG');
        $list = is_array($list) ? $list : array();
        $images = array();
        
        if (!count($list)) { 
            echo 'No artworks found...';
            return false;
        }
        
        foreach ($list as $i => $img) {
            $images[$i] = new JRegistry();
            $images[$i]->set('url', COM_JBMSLIDESHOW_IMAGES_URL.DS.$img);
            $images[$i]->set('path',COM_JBMSLIDESHOW_IMAGES.DS.$img);
            $images[$i]->set('name',JFile::stripExt($img));
            
        }

		$this->assignRef('images', $images);
        $this->assignRef('actions', $actions);

		parent::display($tpl);
	}
}
