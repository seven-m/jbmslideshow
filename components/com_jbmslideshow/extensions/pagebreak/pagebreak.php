<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
require_once (COM_JBMSLIDESHOW_EXTENSIONS.DS.'extension.php');

class JbmslideshowExtensionPagebreak extends JbmslideshowExtension
{
    public function __construct()
    {}
    
    public function display()
    {
        $this->addScript('pagebreak.js');
        $this->addStylesheet('pagebreak.css');
    }
}
