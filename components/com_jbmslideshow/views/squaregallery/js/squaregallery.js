(function ($, window, document) {
	$(function () {
                
        // start the slideshow
        var play = function(API) {
            var $this = $(this),
                o = API.getSettings(),
                $container = $('#content-container');
                
                
            function getOriginalSlide($slide) {
                return $([API.get('contentSelector'), API.get('slideSelector')].join(' '))
                    .filter(function(){ return $(this).attr('slide-num') === $slide.attr('slide-num')})
            }
            
            // build the layout
            if (!$('#jbmslideshow-show').length) {

                // mark the slides for data retrieval
                $([API.get('contentSelector'), API.get('slideSelector')].join(' '), $this).each(function(i,v) {
                    $(this).attr('slide-num',i);
                })
                
                $(o.contentSelector, $this)
                    .before($('.categorylist', $this), $('.row-separator',$this))
                    .clone(true)
                    .attr('id','jbmslideshow-show')
                    .removeAttr('class')
                    .addClass('loading')
                    .insertAfter($(o.contentSelector, $this))
                    .find('.slide-controls').hide()
                    .end().end().hide();
                
                var $show = $('#jbmslideshow-show');
                
                // fix dimensions
                var h = 10, v=8;
                var dims = $('.page', $show).width()/h;
                $('.slide.image, .placeholder', $show)
                    .css('width', 100/h + '%')
                $('.slides',$show).css('height',$('.slide.image',$show).first().width()*v);
                $(window).resize(function() {$('.slides',$show).css('height',$('.slide.image',$show).first().width()*v)});
                
                // add mouseover effect
                $('.slide.image', $show).not('.placeholder,.empty').on('mouseover', function() {
                    var $container = $(this).parents('.page');
                        src = API.getThumbSrc({
                            file: API.getExtensionData(getOriginalSlide($(this)), 'image', 'filename'),
                            folder: API.getExtensionData(getOriginalSlide($(this)),'image','folder'),
                            size: [$('.image-frame',$show).width(), $('.image-frame',$show).width()]
                        }),
                        desc = API.getExtensionData(getOriginalSlide($(this)),'image','description');

                    if ($('.image-frame img', $container).attr('src') == src) return;
                    $('.image-frame', $container).addClass('loading');
                    $('.image-frame img', $container).attr('src', src);
                    $('.image-frame .description', $container).text(desc ? desc.text : '');
                });
                    
                $('.image-frame img', $show)
                    .load(function() {
                        $('.image-frame', $show).removeClass('loading');
                    })
                    .error(function() { 
                        $('.image-frame', $show).removeClass('loading'); 
                        $(this).attr('src',API.get('pluginUrl') + '/images/blank.gif');
                    })
                
                API.swipeBox($('.preview', $show));
            }
            
            $(window).ready(function() {
                var $show = $('#jbmslideshow-show');
                
                if ($('.pagebreak', $show).length <= 1) {
                    $show.removeClass('loading');
                    return;
                }
                
                $('.jbmslideshow-navi', $this)
                    .append($('<a href="#" class="prev"/>'))
                    .append($('<a href="#" class="next"/>'))
                    .delay(1000)

                $show
                    .on('cycle-initialized', function() { $(this).removeClass('loading')})
                    .cycle({
                        next : $('.jbmslideshow-navi .next', $this),
                        prev : $('.jbmslideshow-navi .prev', $this),
                        pauseOnHover : API.get('pause', true, 'boolean'),
                        speed : API.get('speed', 500, 'int'),
                        timeout : 0, //API.get('timeout', 4000, 'int'),
                        slides : '.page',
                        log : false,
                        swipe : true,
                        fx : API.get('effect','fade')
                    })
            })
            
        }
        
        $('#jbmslideshow').bind('jbmslideshow.onStarted', function(event, API) {
            var _this = this;
            play.call(_this, API);
        });
        
    });
} (this.jQuery.noConflict(), this, this.document));        