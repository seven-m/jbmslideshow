<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
/**
 * Script file of jbmslideshow component
 */
class com_jbmslideshowInstallerScript
{
        /**
         * method to install the component
         *
         * @return void
         */
        function install($parent) 
        {
            // $parent is the class calling this method
            // $parent->getParent()->setRedirectURL('index.php?option=com_jbmslideshow');
        }
 
        /**
         * method to uninstall the component
         *
         * @return void
         */
        function uninstall($parent) 
        {
            // $parent is the class calling this method
            // echo '<p>' . JText::_('COM_HELLOWORLD_UNINSTALL_TEXT') . '</p>';
        }
 
        /**
         * method to update the component
         *
         * @return void
         */
        function update($parent) 
        {
            // $parent is the class calling this method
            // echo '<p>' . JText::sprintf('COM_HELLOWORLD_UPDATE_TEXT', $parent->get('manifest')->version) . '</p>';
        }
 
        /**
         * method to run before an install/update/uninstall method
         *
         * @return void
         */
        function preflight($type, $parent) 
        {}
 
        /**
         * method to run after an install/update/uninstall method
         *
         * @return void
         */
        function postflight($type, $parent) 
        {
            // always create or modify these parameters
            $categories = new StdClass();
            $categories->name = 'categories';
            $categories->type = 'extension';
            $categories->published = 1;            
            $categories->params = new StdClass();
            
            $image = new StdClass();
            $image->name = 'image';
            $image->type = 'plugin';
            $image->published = 1;
            $iamge->params = new StdClass();
            
            $pagebreak = new StdClass();
            $pagebreak->name = 'pagebreak';       
            $pagebreak->type = 'plugin';
            $pagebreak->published = 1;
            $pagebreak->params = new StdClass();
            
            $link = new StdClass();
            $link->name = 'link';       
            $link->type = 'extension';
            $link->published = 1;
            $link->params = new StdClass();            
            
            $params['extensions'] = array($categories, $image, $pagebreak, $link);

            $this->setParams( $params );

            echo '<p>' . JText::_('COM_JBMSLIDESHOW_POSTFLIGHT' . $type ) . '</p>';
        }
        private function setParams($param_array) {
            if ( is_array($param_array) && count($param_array) ) {
                // read the existing component value(s)
                $db = JFactory::getDbo();
                $db->setQuery('SELECT params FROM #__extensions WHERE name = "com_jbmslideshow"');
                $params = new JRegistry();
                $params->loadString($db->loadResult());
                
                $new_params = new JRegistry();
                $new_params->loadArray($param_array);
                
                $new_params->merge($params);
                
                // store the combined new and existing values back as a JSON string
                $db->setQuery('UPDATE #__extensions SET params = ' .
                    $db->quote( $new_params->toString() ) .
                    ' WHERE name = "com_jbmslideshow"' );
                    $db->query();
            }
        }
}