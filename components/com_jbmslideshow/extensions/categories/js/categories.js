(function ($, window, document) {
	$(function () {
        
        // extend array
        Array.prototype.unique = function(a){
            return function(){ return this.filter(a) }
        }(function(a,b,c){ return c.indexOf(a,b+1) < 0 });
        
        // Extension Categories
        var CategoriesExtension = {
                onReady : function($slides) {
                    var _this = this,
                        API = this.getAPI(),
                        $select, $head, list= new Array();
                    
                    // global label for uncategorized
                    this.uncatLabel = 'uncategorized';
                    
                    $slides.each(function() {
                        var data = API.getExtensionData(this, _this);
                        if (data && data.category) {
                            list.push(data.category)
                        } else { 
                            // insert slides with no category into uncategorized
                            if ($.inArray(this.uncatLabel, list) < 0) list.unshift(_this.uncatLabel);
                            API.setExtensionData(this, {category: _this.uncatLabel}, _this);
                        }
                    });
                    // save the list
                    this.list = list = list.unique();
                    
                    $selectlist = API.create.selectlist.call(this, 'toolbar', {
                            list : list,
                            onSetItem : this._setCurrCat,
                            onItemClicked : this._setCurrCat,
                            onAddItem : this._addCategory,
                            onDeleteItem : this._delCategory,
                            onEditItem : this._editCategory,
                            onSorted : this._sortCategories,
                            sortable : true,
                            modifiable : true,
                            current : list[0],
                            initialHead : 'edit categories'
                        });
                },
                label : "Categories",
                slideControls : {
                    category : {
                        type : 'icon',
                        title : 'category',
                        path : 'extensions/categories/images/categories.png',
                        clickFn : function($slide, $button) {
                            if (!this._getList().length) {
                                alert('there are no categories available.');
                                return;
                            }
                            if (!$button.find('.selectlist').length) {
                                var $content = $('<div />');
                                var $list = API.create.selectlist.call(this, $content, {
                                        sortable : false,
                                        modifiable : false,
                                        current : this._getCurrCat(),
                                        list : this._getList(),
                                        showHead : false,
                                        collapsible : false,
                                        onItemClicked : function(category) { this._move($slide, category); API.dialog.close(); }
                                    });
                                API.dialog.open({
                                    width: 50,
                                    content: $content,
                                    label: 'Move item into:'
                                })
                            }
                        }
                    }
                },
                onAddSlide : function($slide){
                    var _this = this,
                        API = this.getAPI(),
                        currCat = this._getCurrCat();
                    
                    API.setExtensionData($slide, {category: currCat ? currCat : this.uncatLabel}, _this);                    
                    API.create.icon.call(this, $slide, this.slideControls.category);
                },
                _getList : function() {
                    return this.list;
                },
                _setList : function(list) {
                    if ($.isArray(list))
                        return this.list = list;
                    else return false;
                },
                _delCategory : function(name) {
                    var _this = this,
                        API = this.getAPI(),
                        $slides = API.getAllSlides(),
                        name = $.trim(name);
                    if ($.inArray(name, _this.list) + 1) {
                        _this.list.splice($.inArray(name, _this.list), 1);                        
                        $slides.filter(function(){ return API.getExtensionData($(this), _this).category === name }).remove();
                    }
                },
                _editCategory : function(oldVal, newVal) {
                    var _this = this,
                        API = this.getAPI(),
                        $slides = API.getAllSlides(),
                        name = $.trim(name),
                        index = $.inArray(oldVal, this.list);
                    if (index + 1) {
                        this.list[index] = newVal;
                        API.setExtensionData($slides.filter(function(){ 
                                return API.getExtensionData($(this), _this).category == oldVal 
                            }), {category: newVal}, _this);
                    }
                    this._refresh();
                },
                _addCategory : function(name) {
                    var _this = this,
                        API = this.getAPI(),
                        name = $.trim(name);
                    
                    if (!name || typeof name !== 'string') return;
                    
                    this._getList().push(name);
                    
                    // if first category, assign all slides to it
                    if (this.list.length == 1) {
                        API.getAllSlides().each(function(){
                            API.setExtensionData($(this), {category: name}, _this);
                        });
                        this._setCurrCat(name);
                    }
                },
                _setCurrCat : function(name) {
                    var _this = this,
                        API = this.getAPI(),
                        $slides = API.getAllSlides();
                    
                    name = name ? $.trim(name) : false;
                    if (name) {
                        this.currCat = name;
                        this._refresh();
                    } else {
                        this.currCat = null;
                    }
                },
                _getCurrCat : function() {
                    return this.currCat;
                },
                _sortCategories : function(list) {
                    var _this = this,
                        API = this.getAPI(),
                        $slides = API.getAllSlides();
                    this._setList(list.slice(0));
                    // reorder slides
                    $.each(list.reverse(), function(){
                        var cat = this;
                        $slides.parent().prepend($slides.filter(function() { return API.getExtensionData($(this), _this).category == cat }));
                    });
                },
                _refresh : function() {
                    var _this = this,
                        API = this.getAPI(),
                        currCat = this._getCurrCat() ? this._getCurrCat() : this.uncatLabel,
                        $slides = API.getAllSlides();
                    
                    // show only current category slides
                    $slides.filter(function() {return API.getExtensionData($(this), _this).category != currCat }).hide();
                    $slides.filter(function() {return API.getExtensionData($(this), _this).category == currCat }).show();
                },
                _move : function($slide, category) {
                    var _this = this,
                        API = this.getAPI();
                    API.setExtensionData($slide, {category: $.trim(category)}, _this);
                    this.refresh();
                },
                refresh : function() {
                    this._sortCategories(this._getList());
                    this._refresh();
                }

        } /* end of var CategoriesExtension */
            
        // add plugin
        $("#jbmslideshow").jbmslideshow('addExtension', 'categories', CategoriesExtension);
    
    });
} (this.jQuery, this, this.document));    