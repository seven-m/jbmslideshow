<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2012 Jan B Mwesigwa, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
?>

<div id="jbmslideshow-images">
    <?php  foreach($this->images as $image) : ; ?>
        <div class="image-wrapper">
            <img class="browser-images" src="<?php echo $image->get('url'); ?>" alt="<?php echo $image->get('name',''); ?>" path="<?php echo $image->get('path',''); ?>"/>
        </div>
    <?php endforeach; ?>
</div>