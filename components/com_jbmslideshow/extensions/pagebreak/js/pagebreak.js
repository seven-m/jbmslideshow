(function ($, window, document) {
	$(function () {
                
        // Pagebreak plugin
        var PagebreakPlugin = {
            onReady : function($slides) {
            },
            label : "pagebreak",
            onInsert : function() { 
                var API = this.getAPI(),
                    _this = this;
                API.addSlide.call(this, this, function($slide) { 
                    API.updateSlide($slide, { title: 'pagebreak' });
                    _this._changeTitle.call(_this, $slide);
                });
                
            },
            slideControls : {
                changeTitle : {
                    type : 'icon',
                    title: 'title',
                    clickFn: function($slide) { this._changeTitle.call(this, $slide) },
                    path : 'images/edit.png'
                }
            },
            _changeTitle : function($slide) {
                var _this = this,
                    API = this.getAPI(),
                    data = API.getExtensionData($slide, _this),
                    $content = $('<input type="text" class="field-title"/>').val(data.title); 

                // push all into system dialog
                API.dialog.open.call(_this, {
                    label: "Set pagebreak title",
                    width: 300,
                    height: 100,
                    buttons: {
                        save : function() { 
                            // collect values
                            API.updateSlide($slide, { title: $('.field-title', API.dialog.$dialog).val() }); 
                            API.dialog.close(); 
                        },
                        cancel : function() { API.dialog.close(); }
                    },
                    content: $content
                });
            },
            update : function($slide, data) {
                var API = this.getAPI(),
                    title = data.title ? data.title : '';
                $slide.find('.title').text(title);
            }
        } /* end of var PagebreakPlugin */
            
        // add plugin
        $("#jbmslideshow").jbmslideshow('addExtension', 'pagebreak', PagebreakPlugin);
    
    });
} (this.jQuery, this, this.document));    