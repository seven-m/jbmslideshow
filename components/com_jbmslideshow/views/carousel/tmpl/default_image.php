<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2012 Jan B Mwesigwa, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$thumbUrl = $this->helper->getThumbUrl(
                $this->image->get('data.image.filepath'), 
                array($this->params->get('width',640),$this->params->get('height',400)), 
                null, 
                array('crop'=>$this->image->get('data.image.crop'))
            );
$linkUrl = $this->helper->getThumbUrl(
                $this->image->get('data.image.filepath'), 
                array(1000,1000)
            );
?>

<div class="plugin-image">
    <img src="<?php echo $thumbUrl; ?>" alt="<?php echo $this->image->get('data.image.description.text',''); ?>" />
    <?php if ($this->image->get('data.link.url',false)) : ?>
        <div class="link-wrapper">
            <a class="link" href="<?php echo $this->image->get('data.link.url'); ?>">
                <?php echo JText::_('COM_JBMSLIDESHOW_VIEW_CAROUSEL_SEEMORE'); ?>
            </a>
        </div>
    <?php endif; ?>
</div>