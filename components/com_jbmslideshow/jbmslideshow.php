<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die; 

// Include dependancies 
jimport('joomla.application.component.controller'); 

define('COM_JBMSLIDESHOW_PATH', JPATH_SITE.DS.'components'.DS.'com_jbmslideshow');
define('COM_JBMSLIDESHOW_ADMIN', JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jbmslideshow');
define('COM_JBMSLIDESHOW_URL', JURI::base().'components'.DS.'com_jbmslideshow');
define('COM_JBMSLIDESHOW_EXTENSIONS', JPATH_SITE.DS.'components'.DS.'com_jbmslideshow'.DS.'extensions');
define('COM_JBMSLIDESHOW_EXTENSIONS_URL', JURI::base().'components'.DS.'com_jbmslideshow'.DS.'extensions');

//define('COM_JBMSLIDESHOW_MEDIA', JPATH_SITE.DS.'media'.DS.'jbmslideshow');
//define('COM_JBMSLIDESHOW_MEDIA_URL', JURI::base().DS.'media'.DS.'jbmslideshow');
define('COM_JBMSLIDESHOW_MEDIA', JPATH_SITE.DS.'images');
define('COM_JBMSLIDESHOW_MEDIA_URL', JURI::base().DS.'images');

define('COM_JBMSLIDESHOW_IMAGES', JPATH_SITE.DS.'media'.DS.'jbmslideshow'.DS.'images');
define('COM_JBMSLIDESHOW_IMAGES_URL', JURI::base().'media'.DS.'jbmslideshow'.DS.'images');

$controller = JController::getInstance('Jbmslideshow');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
