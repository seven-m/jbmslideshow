<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2012 Jan B Mwesigwa, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$item = isset($this->item) ? $this->item : new JRegistry();
?>

<div class="plugin-pagebreak">
    <div class="description-wrapper">
        <div class="background"></div>
        <div class="title">
            <?php echo $item->get('data.title', false) ? $item->get('data.title') : 'pagebreak'; ?> 
        </div>
    </div>
</div>