(function ($, window, document) {
	$(function () {
        
        if (!window['YouTubeIframeAPICallbacks']) {
            window['YouTubeIframeAPICallbacks'] = {
                add : function(fn) {
                    this.stack.push(fn);
                },
                stack : [],
                run : function() {
                    $.each(this.stack, function(){
                        this.call();
                    })
                }
            }
        } 
        
        var smallScreen = screen.width < 500;
        
        var redraw = function() {
            $('<style />').appendTo($('head')).remove();
            if (window.opera) {
                var p = $('body').css('position')
                $('body').css('position','');
                setTimeout(function() {$('body').css('position',p)}, 1);
                document.body.style += "";
            }
        }
        
        var getVideoInfo = function(id, callback) {
            var url = 'https://www.googleapis.com/youtube/v3/videos',
                data = {
                    alt: 'json',
                    key: 'AIzaSyA_OGDPgdRNErvxltfGzA2svIzIhFhVk5Q',
                    type: 'video',
                    part: 'snippet',
                    id: id
                },
                result = {
                    title: '',
                    description: ''
                };
            return $.getJSON(url, data, function(data){
                if (data && data.pageInfo.totalResults) {
                    var v = data.items[0];
                    $.extend(result, {
                        title : v.snippet.title,
                        description : v.snippet.description
                    });
                    if (typeof callback == 'function') callback(result);
                } 
            }).fail(function(){ log.error('could not retrieve iformation about video. id: ' + id) })
        }
                
        var getIframeSrc = function(vid) {
            var API = this,
                playerVars = {
                wmode : 'transparent',
                controls : API.get('controls',0),
                autoplay : API.get('autoplay',1),
                modestbranding : 1,
                showinfo : API.get('showinfo',0),
                iv_load_policy : 3,
                rel : API.get('rel',0),
                loop : API.get('loop',1)
            }
                
            // filter playerVars as a solution for touch screens
            if (API.isMobile) {
                $.extend(playerVars, {
                    controls: 1,
                    autoplay: 0
                });
            }
            return 'https://www.youtube.com/embed/' + vid + '/?' + $.param(playerVars);
        }
        
        // build the slideshow
        var build = function($this) {
            var API = this,
                o = this.getSettings();
            
            $([API.get('contentSelector'), API.get('slideSelector')].join(' '), $this).each(function(i,v) {
                $(this).attr('slide-num',i);
            })
            
            $(API.get('contentSelector'), $this)
                .clone(true)
                .attr('id','jbmslideshow-show')
                .css('opacity',0)
                .removeAttr('class')
                .insertAfter($(o.contentSelector, $this))
                .find('.slide-controls').hide()
                .end().end().hide();
            
            var $show = $('#jbmslideshow-show'),
                $slides = $('.slide', $show);
            
            // solution for small screens
            if (smallScreen) {
                $show.addClass('small-screen');
                $slides.remove();
                $('.player', $show).remove();
                var $slides = $([API.get('contentSelector'), API.get('slideSelector')].join(' '), $this),
                    $button = $('<a class="load-more-button" />').text('more works...'),
                    loadSlides = function(start, limit) {
                        $slides.slice(start, start + limit).each(function(i,s) {
                            var videoId = API.getExtensionData($(s),'youtube','videoId'),
                                $c = $('<div class="player-container" />')
                                    .append($('<div class="player" />')
                                        .append($('<iframe></iframe>')
                                            .attr({
                                                src : getIframeSrc.call(API, videoId),
                                                frameborder : 0,
                                                allowfullscreen : 1
                                            })
                                        )
                                    )
                                    .append($('<div />')
                                        .append('<div class="title" />')
                                        .append('<div class="description" /><br />')
                                        .append('<a class="readmore" />')
                                    )
                                    .appendTo($show)
                                
                            getVideoInfo(videoId, function(result){
                                $('.title', $c).text(result.title ? result.title : '');
                                $('.description', $c).text(result.description ? result.description : '');
                                $('.readmore', $c).attr('href', API.getExtensionData($(s),'link','url'))
                                if (API.getExtensionData($(s),'link','url')) 
                                    $('.readmore', $c).show()
                                else $('.readmore', $c).hide();
                            })
                            .fail(function(){
                                $('.title', $c).text(API.getExtensionData($(s),'youtube','title'));
                                $('.description', $c).text(API.getExtensionData($(s),'youtube','description'));
                                $('.readmore', $c).attr('href', API.getExtensionData($(s),'link','url'))
                                if (API.getExtensionData($(s),'link','url')) 
                                    $('.readmore', $c).show()
                                else $('.readmore', $c).hide();
                            });
                        });
                        start = start + limit; 
                        if ($slides.slice(start).length) {
                            $show.append($button).unbind('click').click(function(e) {
                                e.preventDefault();
                                loadSlides(start, limit);
                            });
                        } else {
                            $button.remove();
                        }
                    }
                loadSlides(0, 5);
                $show.animate({opacity: 1});
            } else {
                // set iFrame size
                $('#jbmslideshow_player', $show)
                    .css('width','100%')
                    .css('height', Math.round($('#jbmslideshow_player', $show).width() * 9/16))
            
                // add navigation
                var $navi = $('.jbmslideshow-navi')
                    .append($('<a href="#" class="prev"/>'))
                    .append($('<a href="#" class="next"/>'))
                    .appendTo($('.slides',$show))
                
                // run jcycle
                $('.slides', $show)
                    .on('cycle-initialized', function() {
                        // set slides height 
                        var ratio = 9/16,
                            h = Math.round($slides.first().width() * ratio);
                        $slides.each(function() {
                            $('.youtube-item',$(this)).css({height: h})
                        })
                    })
                    .cycle({
                        next : $('.next', $navi),
                        prev : $('.prev', $navi),
                        timeout : 0,
                        slides : '.page',
                        swipe : true,
                        log : false,
                        fx : 'scrollHorz'
                    })
                    
                var videoId = API.getExtensionData($(API.get('slideSelector'), $(API.get('contentSelector'))).first(),'youtube','videoId');
                
                $('#jbmslideshow_player', $show).attr('src', getIframeSrc.call(API, videoId));
                
                // add fullscreen button click event for non-touch devices
                if (!API.isMobile) {
                    $show.find('.fullscreen-button')
                        .click(function(){
                            $(this).fadeOut();
                        })
                } else {
                    $show.find('.fullscreen-button').hide();
                }
            }
        }
        
        var loadVideo = function(player, $slide) {
            var API = this,
                $slide = $([API.get('contentSelector'), API.get('slideSelector')].join(' '))
                            .filter(function(){ return $(this).attr('slide-num') === $slide.attr('slide-num')}),
                id = API.getExtensionData($slide, 'youtube', 'videoId'),
                $show = $('#jbmslideshow-show');
            
            getVideoInfo(id, function(result) {
                $('.title', $show).text('').text(result.title ? result.title : '');
                $('.description', $show).text('').text(result.description ? result.description : '');
                redraw();
            })
            .fail(function(){
                $('.title', $show).text(API.getExtensionData($slide, 'youtube', 'title'));
                $('.description', $show).text(API.getExtensionData($slide, 'youtube', 'description'));
                redraw();
            });

            player.attr('src', getIframeSrc.call(API, id));
            
            // set iFrame size
            player
                .css('width','100%')
                .css('height', Math.round(player.width() * 9/16))
            
            $slide.parent().find('.current').removeClass('current');
            $slide.addClass('current');
            
            // add readmore link
            if (API.getExtensionData($slide, 'link', 'url')) {
                $('a.readmore', $show)
                    .attr('href', API.getExtensionData($slide, 'link', 'url'))
                    .show()
            } else {
                $('a.readmore', $show).hide();
            }
        }
        
        $('#jbmslideshow').bind('jbmslideshow.onStarted', function(event, API) {
            build.call(API,$(this));
            var _this = this,
                $show = $('#jbmslideshow-show');
            if (!smallScreen) { 
                // on slide click 
                $(API.get('slideSelector'), $show).click(function(){
                    loadVideo.call(API, $('#jbmslideshow_player',$show), $(this));
                })
                // show it
                $('#jbmslideshow_player', $show).animate({opacity:1}, function(){ redraw() });
                $show.animate({opacity:1}, function(){ redraw() });
            }
        });
    });
} (this.jQuery, this, this.document));        