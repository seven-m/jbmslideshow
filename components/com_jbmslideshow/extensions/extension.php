<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.modelform');

/**
 * Jbmslideshow Component slideshow 
 *
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @since 1.5
 */
class JbmslideshowExtension extends JObject
{
    protected static $extensions = array();
        
    public function __construct()
    {
        if (!self::$extensions) {
            $list = JComponentHelper::getParams('com_jbmslideshow')->get('extensions', array());
            $list = is_array($list) ? $list : array();
            foreach($list as $ext) {
                $ext = self::getInstance($ext->name);
            }
        }
    }
    
    /**
	 * Returns a jbmslideshow extension object.
	 *
	 * @param   string   $extension   The extension to use.
	 *
	 * @return  JbmslideshowExtension  The Extension object.
	 *
	 * @since   1.6
	 */
	public static function getInstance($ext)
	{
		if (!isset(self::$extensions[$ext]))
		{
			return self::_load($ext);
		} 
        
        return self::$extensions[$ext];
	}

    protected function _load($ext = null)
    {
        if (!$ext) return false;
        
        $file = COM_JBMSLIDESHOW_EXTENSIONS.DS.$ext.DS.$ext.'.php';
        if (is_file($file)) { 
            require_once($file);
            $class = 'JbmslideshowExtension'.ucfirst(strtolower($ext));
            if (class_exists($class)) {
                $e = new $class($ext);
                $e->settings = array();
                $e->scripts = array();
                $e->stylesheets = array();
                $e->name = $ext;
                $e->display();
                $list = JComponentHelper::getParams('com_jbmslideshow')->get('extensions', array());
                foreach($list as $item) {
                    if ($item->name == $e->name)
                    foreach($item as $key => $prop) {
                        $e->$key = $prop;
                    }
                }
                $e->paramsform = $e->getParamsForm();
                $e->registered = false;
                return self::$extensions[$ext] = $e;
            }
            else return false;
        }
        else return false;
    }
    
    protected function setSettings($settings) 
    {
        if (is_array($settings)) {
            $this->settings[] = $settings;
        }
    }
    
    protected function addScript($path = '') 
    {
        if (preg_match('/^http:/i', $path)) {
            $this->scripts[] = $path;
        } else {
            if (file_exists(COM_JBMSLIDESHOW_EXTENSIONS.DS.$this->name.DS.'js'.DS.$path)) {
                $this->scripts[] = COM_JBMSLIDESHOW_EXTENSIONS_URL.DS.$this->name.DS.'js'.DS.$path;
            } else return false;
        }
        return true;
    }
    
    protected function addStylesheet($path = '') 
    {
        if (preg_match('/^http:/i', $path)) {
            $this->stylesheets[] = $path;
        } else {
            if (file_exists(COM_JBMSLIDESHOW_EXTENSIONS.DS.$this->name.DS.'css'.DS.$path)) {
                $this->stylesheets[] = COM_JBMSLIDESHOW_EXTENSIONS_URL.DS.$this->name.DS.'css'.DS.$path;
            } else return false;
        }
        return true;
    }
    
    public function getScripts()
    {
        return $this->scripts;
    }
    
    public function getStylesheets()
    {
        return $this->stylesheets;
    }
    
    public function getExtensions()
    {
        $result = array();
        foreach (self::$extensions as $name => $ext) {
            if ($ext->isPublished()) $result[$name] = $ext;
        }
        return $result;
    }
    
    public function isPublished()
    {
        return (int) $this->published;
    }

    /**
	 * Method to get EXTENSIONS params as html form
     * returns JForm html
	 * @since	2.5
	 */
	protected function getParamsForm()
	{
        $ext = $this->name;
        $xml = COM_JBMSLIDESHOW_EXTENSIONS.DS.$ext.DS.$ext.'.xml';
        $data = JComponentHelper::getParams('com_jbmslideshow')->get('extensions.'.$ext, false);
        $form = '';
        $html = '';
        
        // Get the form.
		JForm::addFormPath( COM_JBMSLIDESHOW_EXTENSIONS.DS.$ext );
        try
		{
            $form = JForm::getInstance('com_jbmslideshow.extensions', $xml, array(), false, 'config/fields');
			$form->bind($data);
		}
		catch (Exception $e)
		{
			$this->setError($e->getMessage());
			return false;
		}
        
        foreach ($form->getFieldsets('params') as $fieldsets => $fieldset) 
        {
            foreach($form->getFieldset($fieldset->name) as $field) 
            {
                $html .=    $field->hidden ? $field->input : '<span>'.$field->label.'</span>'.'<span>'.$field->input.'</span>'.'<br />';
            }
        }
        
        return $html;
	}
}
