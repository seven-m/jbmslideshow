(function ($, window, document) {
	$(function () {
        // Extension Link
        var LinkExtension = {
                onReady : function($slides) { 
                },
                label : "Link",
                slideControls : {
                    link : {
                        type : 'icon',
                        title : 'link',
                        path : 'images/link.png',
                        clickFn : function($slide, $button) {
                            this._select($slide)
                        }
                    }
                },
                onAddSlide : function($slide){
                    var _this = this,
                        API = this.getAPI();
                    
                    API.setExtensionData($slide, {url: '' }, _this);                    
                    API.create.icon.call(_this, $slide, _this.slideControls.link);
                },
                _select : function($slide) {
                    var _this = this,
                        API = this.getAPI(),
                        data = API.getExtensionData($slide, _this);
                        
                    _this.getView({view:'links'}, function(result) {
                        var $content = $('<div><span class="label url" /><input type="text" class="field-url" placeholder="enter URL or select from below"/><div class="clear" />').append($(result));
                        
                        $('a.item', $content).click(function(e) {
                            e.preventDefault();
                            API.setExtensionData($slide, { url: $(this).attr('href') }, _this); 
                            API.dialog.close(); 
                        });
                        
                        if (data && data.url) $('.field-url', $content).val(data.url);
                        
                        // push all into system dialog
                        API.dialog.open.call(_this, {
                            label: "Set a link",
                            width: 50,
                            height: 90,
                            buttons: {
                                save : function() { 
                                    // collect values
                                    API.setExtensionData($slide, { url: $('.field-url', $content).val() }, _this); 
                                    API.dialog.close(); 
                                },
                                cancel : function() { API.dialog.close(); }
                            },
                            content: $content
                        });
                    })
                }

        } /* end of LinkExtension */
            
        // add plugin
        $("#jbmslideshow").jbmslideshow('addExtension', 'link', LinkExtension);
    
    });
} (this.jQuery, this, this.document));    