(function ($, window, document) {
	$(function () {
                
        // Youtube plugin
        var YoutubePlugin = {
            onReady : function($slides) {
            },
            label : "youtube",
            onInsert : function() { 
                var API = this.getAPI(),
                    _this = this;
                API.addSlide.call(this, this, function($slide) { 
                    API.updateSlide($slide, { url: '' });
                    _this._selectVideo.call(_this, $slide, true, true);
                });
                
            },
            slideControls : {
                selectVideo : {
                    type : 'icon',
                    title: 'select video',
                    clickFn: function($slide) { this._selectVideo.call(this, $slide) },
                    path : 'extensions/youtube/images/select.png'
                }/*,
                editSettings : {
                    type : 'icon',
                    title: 'settings',
                    clickFn: function($slide) { this._editSettings.call(this, $slide) },
                    path : 'images/edit.png'
                }
                */
            },
            _selectVideo : function($slide, multipleSelect, newSlide) {
                var _this = this,
                    API = this.getAPI(),
                    $searchInput = $('<input type="text" />').addClass('search-input').attr('placeholder','enter search keywords or video URL'),
                    $result = $('<div />').addClass('result'),
                    $loadMore = $('<a href="#load-more"/>').addClass('load-more button').text('Load more...').hide(),
                    $content = $('<div />').append($searchInput).append($result).append($loadMore);
                
                var load = function(pageToken) {
                    var url = 'https://www.googleapis.com/youtube/v3/search',
                        data = {
                            alt: 'json',
                            q: $searchInput.val(),
                            key: 'AIzaSyA_OGDPgdRNErvxltfGzA2svIzIhFhVk5Q',
                            type: 'video',
                            order: 'date',
                            part: 'snippet'
                        }
                        if (pageToken) data['pageToken'] = pageToken;
                    $.getJSON(url, data, function(data){
                        if (data && data.pageInfo.totalResults > 0) {
                            $loadMore.data('nextPageToken', data.nextPageToken);
                            $.each(data.items, function(i, v) {
                                var thumbPath = v.snippet.thumbnails.default.url,
                                    $img = $('<img />').attr('src', thumbPath),
                                    $item = $('<div class="item">')
                                        .append($img)
                                        .append($('<div class="data" />')
                                            .append('<div class="bg"/>')
                                            .append($('<div class="wrapper">')
                                                .append($('<span class="label"/>').text('Title: ').after('<span>' + v.snippet.title + '</span><br />'))
                                                .append($('<span class="label" />').text('Author: ').after('<span>' + v.snippet.channelTitle + '</span><br />'))
                                                .append($('<span class="label" />').text('Published: ').after('<span>' + v.snippet.publishedAt + '</span><br />'))
                                                .append($('<span class="label" />').html('<span>Select thumbnail: </span><br />').append(function() {
                                                    var $thumbs = $('<span />');
                                                    $.each(v.snippet.thumbnails, function(i,t){
                                                            if (!t.url) return;
                                                            $thumbs.append($('<a class="thumbnail-preview" style="background-image:url('+t.url+')" />') 
                                        .click(function() {
                                            API.updateSlide($slide, {
                                                thumbimage: t.url,
                                                author: v.snippet.channelTitle,
                                                title: v.snippet.title,
                                                description: v.snippet.description,
                                                url: '',
                                                videoId: v.id.videoId,
                                                published: v.snippet.publishedAt
                                            });
                                            API.dialog.close();
                                        }));
                                                    })
                                                    return $thumbs;
                                                }))
                                            )
                                        );
                                $result.append($item);
                                $loadMore.show();
                            })
                        } else {
                            $loadMore.hide();
                        }
                    })
                    .fail(function(){ $result.append('<span>No results...</span>'); $loadMore.hide(); })
                }
                
                // on load more
                $loadMore.click(function(e){
                    e.preventDefault();
                    load($loadMore.data('nextPageToken'));
                })
                
                // on search input update
                $searchInput.change(function() {
                    $result.empty();
                    load();
                });
                
                // on thumbnail selection
                $('.result .item', $content).live('click', function() {
                    API.updateSlide($slide, {thumbimage: $('img', this).attr('src')});
                    API.dialog.close();
                });

                // push all into system dialog
                API.dialog.open.call(_this, {
                    label: "Select video",
                    width: 50,
                    height: 70,
                    buttons: {
                        cancel : function() { if(newSlide) API.removeSlides($slide); API.dialog.close(); }
                    },
                    content: $content
                });    
            },
            _editSettings : function($slide) {
                var _this = this,
                    API = this.getAPI(),
                    data = API.getExtensionData($slide, _this),
                    $content = $('<input type="text" class="field-url"/>').val(data.url); 

                // push all into system dialog
                API.dialog.open.call(_this, {
                    label: "Edit settings",
                    width: 50,
                    height: 50,
                    buttons: {
                        save : function() { 
                            // collect values
                            API.updateSlide($slide, { url: $('.field-url', API.dialog.$dialog).val() }); 
                            API.dialog.close(); 
                        },
                        cancel : function() { API.dialog.close(); }
                    },
                    content: $content
                });
            },
            update : function($slide, data) {
                var API = this.getAPI();
                //data.thumbimage = 'http://i1.ytimg.com/vi/' + data.videoId + '/maxresdefault.jpg';
                //var altSrc = data.thumbimage.replace('maxresdefault.jpg','default.jpg');
                $slide.find('img')
                    .attr({'src':data.thumbimage, alt:data.title})
                    .css({width: API.get('width')})
            }
        } /* end of var YoutubePlugin */
            
        // add plugin
        $("#jbmslideshow").jbmslideshow('addExtension', 'youtube', YoutubePlugin);
    
    });
} (this.jQuery, this, this.document));    