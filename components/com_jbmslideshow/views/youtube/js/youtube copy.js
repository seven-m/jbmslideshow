(function ($, window, document) {
	$(function () {
        
        if (!window['YouTubeIframeAPICallbacks']) {
            window['YouTubeIframeAPICallbacks'] = {
                add : function(fn) {
                    this.stack.push(fn);
                },
                stack : [],
                run : function() {
                    $.each(this.stack, function(){
                        this.call();
                    })
                }
            }
        } 
        
        var smallScreen = screen.width < 500;
        
        var redraw = function() {
            $('<style />').appendTo($('head')).remove();
        }
        
        var getVideoInfo = function(id, callback) {
            var url = 'https://gdata.youtube.com/feeds/api/videos/' + id,
                data = {
                    v: 2,
                    alt: 'json'
                },
                result = {
                    title: '',
                    description: ''
                };
            
            $.getJSON(url, data, function(data){
                if (data && data.entry) {
                    var v = data.entry;
                    $.extend(result, {
                        title : v.title.$t,
                        description : v.media$group.media$description.$t
                    });
                    if (typeof callback == 'function') callback(result);
                } 
            }).fail(function(){ log.error('could not retrieve iformation about video. id: ' + id) })
        }
        
        // build the slideshow
        var build = function(API) {
            var $this = $(this),
                o = API.getSettings();
            
            $([API.get('contentSelector'), API.get('slideSelector')].join(' '), $this).each(function(i,v) {
                $(this).attr('slide-num',i);
            })
            
            $(API.get('contentSelector'), $this)
                .clone(true)
                .attr('id','jbmslideshow-show')
                .css('opacity',0)
                .removeAttr('class')
                .insertAfter($(o.contentSelector, $this))
                .find('.slide-controls').hide()
                .end().end().hide();
            
            var $show = $('#jbmslideshow-show'),
                $slides = $('.slide', $show),
                playerVars = {
                    wmode : 'transparent',
                    controls : API.get('controls',0),
                    autoplay : API.get('autoplay',1),
                    modestbranding : 1,
                    showinfo : API.get('showinfo',0),
                    iv_load_policy : 3,
                    rel : API.get('rel',0),
                    loop : API.get('loop',1)
                }
                
            // filter playerVars as a solution for touch screens
            if (API.isMobile) {
                $.extend(playerVars, {
                    controls: 1,
                    autoplay: 0
                });
            }
                
            // solution for small screens
            if (smallScreen) {
                $show.addClass('small-screen');
                $slides.remove();
                var $slides = $([API.get('contentSelector'), API.get('slideSelector')].join(' '), $this),
                    $button = $('<a class="load-more-button" />').text(API.lang('youtube_load_more_button', 'more works...')),
                    loadSlides = function(start, limit) {
                        $slides.slice(start, start + limit).each(function(i,s) {
                            var videoId = API.getExtensionData($(s),'youtube','videoId'),
                                $c = $('<div class="player-container" />')
                                    .append($('<div class="player" />')
                                        .append($('<iframe></iframe>')
                                            .attr({
                                                src : 'http://www.youtube.com/embed/' + videoId + '/?' + $.param(playerVars),
                                                frameborder : 0,
                                                allowfullscreen : 1
                                            })
                                        )
                                    )
                                    .append($('<div />')
                                        .append('<div class="title" />')
                                        .append('<div class="description" /><br />')
                                        .append('<a class="readmore" />')
                                    )
                                    .appendTo($show)
                                
                            getVideoInfo(videoId, function(result){
                                $('.title', $c).text(result.title ? result.title : '');
                                $('.description', $c).text(result.description ? result.description : '');
                                $('.readmore', $c)
                                    .attr('href', API.getExtensionData($(s),'link','url'))
                                    .text(function() { return API.getExtensionData($(s),'link','url') ? API.lang('youtube_readmore','Read more...') : '' });
                            });
                        });
                        start = start + limit; 
                        if ($slides.slice(start).length) {
                            $show.append($button).unbind('click').click(function(e) {
                                e.preventDefault();
                                loadSlides(start, limit);
                            });
                        } else {
                            $button.remove();
                        }
                    }
                loadSlides(0, 5);
                $show.animate({opacity: 1});
            } else {
                // form the layout
                $show
                    .wrapInner('<div class="slides" />')
                    .prepend('<div class="player"><span class="fullscreen-button" /><iframe id="jbmslideshow_player" allowfullscreen="1" frameborder="0" src="" /><div class="title" /><div class="description" /><a href="#readmore" class="readmore"></a></div>')
                /* 
                Create slides pagination
                */
                var $slides = $('.slide', $show),
                    numSlides = $slides.length,
                    perPage = API.get('slidesPerPage'), page = 0;
                
                // group slides
                for (var i = 0, page=0; i < numSlides; i+=perPage, page++){
                    $slides.filter(':eq('+i+'),:lt('+(i+perPage)+'):gt('+i+')').wrapAll('<div class="page" rel="page'+page+'"/>');
                }
                
                // add navigation
                var $navi = $('.jbmslideshow-navi')
                    .append($('<a href="#" class="prev"/>'))
                    .append($('<a href="#" class="next"/>'))
                    .appendTo($('.slides',$show))
                
                // run jcycle
                $('.slides', $show)
                    .cycle({
                        next : $('.next', $navi),
                        prev : $('.prev', $navi),
                        timeout : 0,
                        slides : '.page',
                        swipe : true,
                        log : false,
                        fx : 'scrollHorz'
                    })
                
                var videoId = API.getExtensionData($(API.get('slideSelector'), $(API.get('contentSelector'))).first(),'youtube','videoId');
                $('#jbmslideshow_player', $show).attr('src', 'http://www.youtube.com/embed/' + videoId + '/?' + $.param(playerVars));
                            
                // add fullscreen button for non-touch devices
                if (!API.isMobile) {
                    $show.find('.fullscreen-button')
                        .text(API.lang('youtube_dbl_click_for_fullscreen'))
                        .click(function(){
                            $(this).fadeOut();
                        })
                }
            }
        }
        
        var destroy = function(API) {
            var $this = $(this);
            $('#jbmslideshow-show', $this).remove();
            $('#jbmslideshow-content', $this).show();
        }
        
        var loadVideo = function(player, $slide) {
            var $slide = $([API.get('contentSelector'), API.get('slideSelector')].join(' '))
                            .filter(function(){ return $(this).attr('slide-num') === $slide.attr('slide-num')}),
                id = API.getExtensionData($slide, 'youtube', 'videoId'),
                $show = $('#jbmslideshow-show');
            getVideoInfo(id, function(result) {
                $('.title', $show).text('').text(result.title ? result.title : '');
                $('.description', $show).text('').text(result.description ? result.description : '');
                redraw();
            }); 
            
            if (API.isMobile) {
                player.cueVideoById(id);
            } else {
                player.loadVideoById(id);
            }
            
            $slide.parent().find('.current').removeClass('current');
            $slide.addClass('current');
            
            // add readmore link
            if (API.getExtensionData($slide, 'link', 'url')) {
                $('a.readmore', $show)
                    .text(API.lang('youtube_readmore','Read more...'))
                    .attr('href', API.getExtensionData($slide, 'link', 'url'))
            } else {
                $('a.readmore', $show).text('');
            }
        }
        
        $('#jbmslideshow').bind('jbmslideshow.onStarted', function(event, API) {
            var _this = this;
            build.call(_this, API);
            if (!smallScreen) {
                var iFrameReady = function() {
                    var $show = $('#jbmslideshow-show'),
                        player = new YT.Player('jbmslideshow_player', {
                            events : {
                                onReady: function(event) {
                                    // set iFrame size
                                    var w = $('#jbmslideshow_player', $show).css('width','100%').width(),
                                        h = Math.round(w * 9/16);
                                    
                                    event.target.setSize('100%',h);
                                    
                                    loadVideo(player, $(API.get('slideSelector'), $show).first()); 
                                    
                                    // on slide click 
                                    $(API.get('slideSelector'), $show).click(function(){
                                        loadVideo(player, $(this));
                                    })
                                    // show it
                                    $('#jbmslideshow_player', $show).animate({opacity:1});
                                    $show.animate({opacity:1});
                                    redraw();
                                },
                                onError : function(event) {
                                    log.error('player error: ' + event.data);
                                },
                                onStateChange : function(event) {
                                    switch(event.data) {
                                        case YT.PlayerState.ENDED : 
                                            var $next = $('.current', $how).next(),
                                                $first = $(API.get('slideSelector'), $how).first();
                                            if ($next.length) {
                                                loadVideo(player, $next);
                                            } else {
                                                loadVideo(player, $first);
                                            }
                                            break;
                                        case YT.PlayerState.PLAYING : 
                                            break;
                                        case YT.PlayerState.PAUSED : 
                                            break;
                                        case YT.PlayerState.BUFFERING :
                                            break;
                                    }
                                }
                            }
                        });
                        if (typeof player.playVideo == 'undefined') {
                            // set iFrame size
                            var w = $('#jbmslideshow_player', $show).width(),
                                h = Math.round(w * 9/16);
                            
                            $('#jbmslideshow_player', $show)
                                .css('width','100%')
                                .css('height',h)
                                
                            loadVideo(player, $(API.get('slideSelector'), $show).first()); 
                            
                            // on slide click 
                            $(API.get('slideSelector'), $show).click(function(){
                                loadVideo(player, $(this));
                            })
                            // show it
                            $('#jbmslideshow_player', $show).animate({opacity:1});
                            $show.animate({opacity:1});
                            redraw();
                        };
                }
                window['YouTubeIframeAPICallbacks'].add(iFrameReady);
                if (!window['onYouTubeIframeAPIReady']) {
                    window['onYouTubeIframeAPIReady'] = function() {
                        window['YouTubeIframeAPICallbacks'].run();
                    }
                    $.getScript('https://www.youtube.com/iframe_api');
                } else if (typeof YT != 'undefined') {
                    iFrameReady();
                } 
            }
        });
        
        $('#jbmslideshow').bind('jbmslideshow.onPreviewStart', function(event, API) {
            build.call(this, API);
        });
        
        $('#jbmslideshow').bind('jbmslideshow.onPreviewStop', function(event, API) {
            destroy.call(this, API);
        });
        
    });
} (this.jQuery, this, this.document));        