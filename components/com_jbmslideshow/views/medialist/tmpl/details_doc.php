<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
$user = JFactory::getUser();
$params = new JRegistry;
$dispatcher	= JDispatcher::getInstance();
$dispatcher->trigger('onContentBeforeDisplay', array('com_jbmslideshow.file', &$this->_tmp_doc, &$params));
?>
		<tr>
			<td>
				<a href="#" class="doc" title="<?php echo $this->_tmp_doc->name; ?>">
					<?php  echo JHtml::_('image', $this->_tmp_doc->icon_16, $this->_tmp_doc->title, null, true, true) ? JHtml::_('image', $this->_tmp_doc->icon_16, $this->_tmp_doc->title, array('width' => 16, 'height' => 16), true) : JHtml::_('image', 'media/con_info.png', $this->_tmp_doc->title, array('width' => 16, 'height' => 16), true);?> </a>
			</td>
			<td class="description"  title="<?php echo $this->_tmp_doc->name; ?>">
				<a href="javascript:window.parent.JbmslideshowMediaManager.onFileSelected('<?php echo $this->_tmp_doc->name.'\',\''.$this->_tmp_doc->path_relative.'\''; ?>)" class="doc" title="<?php echo $this->_tmp_doc->name; ?>">
                    <?php echo $this->_tmp_doc->title; ?>
                </a>
			</td>
			<td>&#160;

			</td>
			<td class="filesize">
				<?php echo MediaHelper::parseSize($this->_tmp_doc->size); ?>
			</td>
			<td>
				<input type="checkbox" name="selection[]" onclick="javascript:window.parent.JbmslideshowMediaManager.onMultipleFilesSelected('<?php echo $this->_tmp_img->name.'\',\''.$this->_tmp_img->path_relative.'\''; ?>)" value="<?php echo $this->_tmp_img->name; ?>" />
			</td>
		</tr>
<?php
$dispatcher->trigger('onContentAfterDisplay', array('com_jbmslideshow.file', &$this->_tmp_doc, &$params));
?>
