<?php
/**
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2005 - 2012 Jan B Mwesigwa
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
jimport('joomla.application.component.helper');
jimport('joomla.application.component.view');

jimport('joomla.registry.registry');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');
jimport( 'joomla.utilities.utility');
require_once(COM_JBMSLIDESHOW_PATH .DS. 'views' .DS. 'jbmslideshowview.php');
/**
 * Content Component Controller
 *
 * @subpackage	com_jbmslideshow
 * @since		1.5
 */
class JbmslideshowController extends JController
{	
    public static $helper = null;
    
    /**
	 * Method to display a view.
	 *
	 * @param	boolean			If true, the view output will be cached
	 * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{

        // TODO disable cache 
        
        $user = &JFactory::getUser();
		$cachable = $user->id ? false : true;
        
        // Set the default view name and format from the Request.
        $vName	= JRequest::getCmd('view', 'slideshow');
		JRequest::setVar('view', $vName);

        // redirect to a non-core view
        $coreViews = array('mediaList');
        if (in_array($vName, $coreViews)) {
            return parent::display($cachable, array('Itemid'=>'INT'));
        }
        
        $app		= JFactory::getApplication();
        $menus      = $app->getMenu();
		$menu       = $menus->getActive();
        $document   = & JFactory::getDocument();
        $helper     = $this->getHelper();
        $actions    = $helper->getActions();
        $params     = $app->getParams();
        $extensions = $helper->getExtensions();
        $language   = & JFactory::getLanguage();
        $logged = $user->id ? true : false;
        
        // check alias menu item
        if ((int) $menu->params->get('use_alias')) {
            JRequest::setVar('Itemid', (int) $menu->params->get('alias_id'));
        }
        
        // load view and model 
		$view = $this->getView($vName,'html');
        $view->addTemplatePath(COM_JBMSLIDESHOW_PATH.DS.'views'.DS.$view->getName().DS.'tmpl');
        $view->addHelperPath(COM_JBMSLIDESHOW_PATH.DS.'helpers');
        $model = JModel::getInstance('Slideshow', 'JbmslideshowModel');
        $view->setModel($model);
    
        // get data  
        $data = $model->getData();
        $content = json_decode(isset($data->content) ? $data->content : '') ? json_decode($data->content, true) : array();
        
        // add slideshow options for js
        $menu->params->set('baseUrl', JURI::base());
        $menu->params->set('basePath', JPATH_SITE);
        $menu->params->set('pluginUrl', COM_JBMSLIDESHOW_URL);
        $menu->params->set('mediaUrl', COM_JBMSLIDESHOW_MEDIA_URL);
        $menu->params->set('mediaPath', COM_JBMSLIDESHOW_MEDIA);
        $menu->params->set('DS', DS);
        $menu->params->set('language', strtolower($language->getTag()));
        $menu->params->set('Itemid', isset($data->Itemid) ? $data->Itemid : JRequest::getVar('Itemid'));
        $menu->params->set('id', isset($data->id) ? $data->id : 0);
        $menu->params->set('extensions', $extensions);
        $menu->params->set('sessionToken', JUtility::getToken());
        $slideshowOptions = json_encode($menu->params->toArray());
        
        // load manager js
        if ($actions->get('core.admin')) {
            $document->addScript(COM_JBMSLIDESHOW_URL.DS.'js'.DS.'plupload'.DS.'js'.DS.'plupload.full.js');
            $document->addScript(COM_JBMSLIDESHOW_URL.DS.'js'.DS.'plupload'.DS.'js'.DS.'plupload.browserplus.js');
            $document->addScript(COM_JBMSLIDESHOW_URL.DS.'js'.DS.'plupload'.DS.'js'.DS.'jquery.ui.plupload'.DS.'jquery.ui.plupload.js');
            $document->addScript(COM_JBMSLIDESHOW_URL.DS.'js'.DS.'manager.js');
            $document->addScriptDeclaration('
                (function ($, window, document) {
                    $(function () {
                        $("#jbmslideshow").bind("jbmslideshow.onReady", function() { $(this).jbmslideshowManager() });
                    });
                } (this.jQuery, this, this.document));        
            ');
        }
        
        // load main and layout related css
        $document->addStylesheet(COM_JBMSLIDESHOW_URL.DS.'css'.DS.'jbmslideshow.css');
        $document->addStylesheet(COM_JBMSLIDESHOW_URL.DS.'views'.DS.$view->getName().DS.'css'.DS.$view->getName().'.css');
        
        // load main and layout related js
        $document->addScript(COM_JBMSLIDESHOW_URL.DS.'js'.DS.'slideshow.js');
        $document->addScript(COM_JBMSLIDESHOW_URL.DS.'views'.DS.$view->getName().DS.'js'.DS.$view->getName().'.js');
        
        $document->addScriptDeclaration('
            (function ($, window, document) {
                $(function () {
                    $("#jbmslideshow").jbmslideshow('.$slideshowOptions.');
                });
            } (this.jQuery, this, this.document));        
        ');
        
        // load extensions js and css
        foreach ($extensions as $ext) {
            foreach($ext->getScripts() as $s) {
                $document->addScript($s);
            }
            foreach($ext->getStylesheets() as $s) {
                $document->addStylesheet($s);
            }
        }
        
        // load view language
        $path = COM_JBMSLIDESHOW_PATH.DS.'views'.DS.$view->getName();
        $language->load($view->getName(), $path, null, true, true);
        
		$view->assignRef('params', $params);
		$view->assignRef('content', $content);
        $view->assignRef('actions', $actions);
        $view->assignRef('logged', $logged);
        $view->assignRef('helper', $helper);
        
        echo '<div id="jbmslideshow" class="jbmslideshow-container '.$view->params->get('pageclass_sfx').'">';
        if ($view->params->get('show_page_heading', 1)) {
            echo '<h1>';
                if ($view->escape($view->params->get('page_heading'))) {
                        echo $view->escape($view->params->get('page_heading'));
                } else {
                        echo $view->escape($view->params->get('page_title'));
                }
            echo '</h1>';
        }

            if ($view->actions->get('core.admin')) {
                echo '
                    <div class="toolbar">
                        <div class="buttons"></div>
                        <div class="tools"></div>
                    </div>
                    <div class="clear"></div>
                ';
            }
            echo '<div class="content-wrapper">
                    <div class="jbmslideshow-navi"></div>
                    <div class="jbmslideshow-content">
            ';
            
            if ($logged && ($actions->get('core.admin'))) {
                foreach ($content as $item) {
                    $view->displaySlide($item);
                }
            } else {
                $view->display();
            }
        echo '</div></div></div>';
	}
    
    /*
    * Get helper
    */
    private function getHelper() 
    {
        if (self::$helper) {
            return self::$helper;
        } else {
            require_once(COM_JBMSLIDESHOW_ADMIN .DS. 'helpers'.DS.'jbmslideshow.php');
            return self::$helper = new JbmslideshowHelper();
        }
    }
    
    public function saveSettings()
    {
        $helper = $this->getHelper();
        $args = JRequest::getVar('args',array());
        
        if (!$helper->getActions()->get('core.admin')) {
            $helper->setResultError('No access');
            die($helper->getResult());
        }
        $params = JComponentHelper::getParams('com_jbmslideshow');
        
        $u = & JURI::getInstance( '?'.$args );
        $new = $u->getQuery(true);
        $params->set('settings', $new['params']);
        
        $component = JComponentHelper::getComponent('com_jbmslideshow');
        $table = JTable::getInstance('extension');
        $table->load( $component->id ); 

        $table->bind( array('params'=>$params->toString()) );
        // pre-save checks
        if (!$table->check()) {
           JError::raiseWarning( 500, $table->getError() );
           return false;
        }

        // save the changes
        if (!$table->store()) {
           JError::raiseWarning( 500, $table->getError() );
           return false;
        }
        $result = $params->toArray();
        
        $helper->setResult($result['settings']);
        die($helper->getResult());
    }
    
    public function getLayout()
    {
        jimport('joomla.application.component.view');
        $helper = $this->getHelper();
        
        if (!$helper->getActions()->get('core.admin')) {
            $helper->setResultError('No access');
            die($helper->getResult());
        }
        
        $args = JRequest::getVar('args',array());
        $layout = isset($args['layout']) ? $args['layout'] : '';
        $path = COM_JBMSLIDESHOW_EXTENSIONS.DS.$layout.DS.'views'.DS.$layout.'.php';
        
        if (!$layout || !file_exists($path)) { 
            $helper->setResultError('Layout "'.$layout.'" does not exist on path '.$path);
            die($helper->getResult());
        }
        
        ob_start();
            include($path);
            $output = ob_get_contents();
        ob_end_clean();
        
        $helper->setResult(array('html'=>$output));
        die($helper->getResult());
    }
    
    public function getExtensionView()
    {
        $user = &JFactory::getUser();
		$cachable = $user->id ? false : true;

		// Set the view name and format from the Request.
        $args = JRequest::getVar('args',array());
        $ext = isset($args['extension']) ? $args['extension'] : '';
        $format = isset($args['format']) ? $args['format'] : 'raw';
        $layout = isset($args['layout']) ? $args['layout'] : null;
        $view = isset($args['view']) ? $args['view'] : '';
        $path = COM_JBMSLIDESHOW_EXTENSIONS.DS.$ext;
        
        $this->addViewPath($path.DS.'views');
        $view = $this->getView( $view, $format, 'JbmslideshowExtension'.ucfirst($ext).'View', array(
            'base_path' => $path,
            'name' => $view, 
            'type' => $format,
            'view_path' => $path,
            'model_path' => $path
            )
        );
        
        $view->display($layout);
    }
        
    /*
    * returns data for all installed and published extensions
    * returns JSON result:{ ext: data }
    */
    public function getExtensionData() 
    {
        require_once(COM_JBMSLIDESHOW_EXTENSIONS.DS.'extension.php');
        $helper = $this->getHelper();
        
        $result = JbmslideshowExtension::getExtensionData();
        
        $helper->setResult($result);
        die($helper->getResult());
    }
        
    public function saveExtensionSettings()
    {
        $helper = $this->getHelper();
        $args = JRequest::getVar('args',array());
        
        if (!$helper->getActions()->get('core.admin')) {
            $helper->setResultError('No access');
            die($helper->getResult());
        }
        $params = JComponentHelper::getParams('com_jbmslideshow');
        
        foreach ($args as $name => $uri) {
            $u = & JURI::getInstance( '?'.$uri );
            $new = $u->getQuery(true);
            $params->set('extensions.'.$name.'.params', $new['params']);
        }
        
        $component = JComponentHelper::getComponent('com_jbmslideshow');
        $table = JTable::getInstance('extension');
        $table->load( $component->id ); 

        $table->bind( array('params'=>$params->toString()) );
        // pre-save checks
        if (!$table->check()) {
           JError::raiseWarning( 500, $table->getError() );
           return false;
        }

        // save the changes
        if (!$table->store()) {
           JError::raiseWarning( 500, $table->getError() );
           return false;
        }
        $result = $params->toArray();
        
        $helper->setResult($result['plugins']);
        die($helper->getResult());
    }
    
    public function getFileList()
    {
        jimport('joomla.filesystem.file');
        jimport('joomla.filesystem.folder');
        $args = JRequest::getVar('args');
        $helper = $this->getHelper();
        
        if (!$helper->getActions()->get('core.admin')) {
            $helper->setResultError('No access');
            die($helper->getResult());
        }
        
        $folder = isset($args['folder']) ? $args['folder'] : '';
        $filter = isset($args['filter']) ? $args['filter'] : '';
        $dir = COM_JBMSLIDESHOW_MEDIA.DS.$folder;
        $files = JFolder::files($dir, $filter, 1);

        $result = array();
        if ($files && is_array($files)) {
            foreach ($files as $key => $file) {
                $result[$key]['folder'] = $folder;
                $result[$key]['filename'] = $file;
                $result[$key]['ext'] = JFile::getExt($file);
                $result[$key]['filesize'] = $this->human_filesize(@filesize($dir.DS.$file));
                list($width, $height, $type, $attr) = @getimagesize($dir.DS.$file);
                $result[$key]['dimensions'] = $width.' x '.$height;
            }
        }
        $helper->setResult($result);
        die($helper->getResult());
    }
    private function human_filesize($bytes, $decimals = 2) 
    {
      $sz = 'BKMGTP';
      $factor = floor((strlen($bytes) - 1) / 3);
      return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }
    
    /**
    * Method to delete a file
    * @args = array of absolute file paths to delete
    * return JSON
    */
    public function deleteFiles() 
    {
        jimport('joomla.filesystem.file');
        $args = JRequest::getVar('args');
        $helper = $this->getHelper();
        
        if (!$helper->getActions()->get('core.admin')) {
            $helper->setResultError('No access');
            die($helper->getResult());
        }
        
        // now loop through files and delete them
        $result = array();
        $error = array();
        if(is_array($args)) {
            foreach ($args as $key=>$path) {
                if (JFile::delete($path)) { 
                    $result['files'][$key]['path'] = $path;
                } else {
                    $error['files'][$key]['path'] = $path;
                }
            }
        } else { 
            $helper->setResultError('No files to delete');
            die(json_encode($helper->getResult()));
        }
        
        if (count($error)) $helper->setResultError('Failed to delete some files', $error);
        $helper->setResult($result);
        die($helper->getResult());
    }    

    
    /**
    * Method to save content
    * returns jsonrpc
    */
    public function save()
    {   
        $args = JRequest::getVar('args');
        $helper = $this->getHelper();        
        $row = $helper->getTable();
        
        // check values before saving into db
        if (!isset($args['Itemid']) || !$args['Itemid'] || $args['Itemid'] == '') {
            $helper->setResultError('Failed to save: incorrect Itemid supplied');
            die($helper->getResult());
        }
        
        $content = new JRegistry();
        if (!$content->loadArray(isset($args['content']) ? $args['content'] : array())) {
            $helper->setResultError('Failed to save: invalid content supplied');
            die($helper->getResult());
        }
        $args['content'] = $content->toString();
        $args['id'] = isset($args['id']) ? $args['id'] : 0;
        
        if (!$row->bind( $args )) { 
            $helper->setResultError('Failed to bind data: '.$row->getError());
            die($helper->getResult());
        }
        
        if (!$row->store()) {
            $helper->setResultError('Failed to store data: '.$row->getError());
            die($helper->getResult());
        } else {
            $helper->setResult(array('saved' => true));
            die($helper->getResult());
        }
    }
    
    /*
    * Method to check if file exists
    * returns json true or false
    */
    public function fileExists()
    {
        $args = JRequest::getVar('args',array());
        $path = JPATH_SITE.DS.array_shift($args);
        if (is_file($path)) { 
            die('{"jsonrpc" : "2.0", "result" : {"path" : "'.$path.'"} }'); 
        } else {
            die('{"jsonrpc" : "2.0", "error" : {"code": 0, "message": "File does not exist: '.$path.'"}}');
        }
    }
    
    /*
    * upload files
    */
    public function upload()
    {        
      //JRequest::checkToken() or die( json_encode('Invalid Token') );
        $files = array();
        $result = array();
        $helper = $this->getHelper();
        
        $helper->upload();
    }
 }
