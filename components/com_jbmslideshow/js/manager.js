/* jbmslideshow manager functions */
(function ($, window, document) { 
    $(function() {
    
        // add event for "enter" key
        $.fn.onEnter = function(func) {
            this.bind('keypress', function(e) {
                if (e.keyCode == 13) func.apply(this, [e]);    
            });               
            return this; 
        };
        
        var log = {
            debug : true,
            error : function(message) {
                var m = 'jbmslideshowManager error';
                try {
                  if (this.debug && window.console && window.console.error){
                    window.console.error([m, message].join(': '));
                  }
                } catch (e) {
                  // no console available
                }
            },
            info : function(message) {
                var m = 'jbmslideshowManager notice';
                try {    
                    if (window.console && window.console.info){
                        window.console.info([m, message].join(': '));
                    }
                } catch (e) {
                  //  console.log(e);
                  // no console available
                }
            }
        }
        var uploader = new plupload.Uploader({
                // General settings
                runtimes : 'html5,gears,silverlight,flash',
                browse_button: 'jbmslideshow-upload-button',
                container : 'jbmslideshow-uploader',
                url : 'index.php?option=com_jbmslideshow&view=player&task=upload',
                max_file_size : '2048kb',
                chunk_size : '1mb',
                unique_names : false,
                upload_dir : 'images',
                
                // Specify what files to browse for
                filters : [
                    {title : "Image files", extensions : 'jpg,jpeg,gif,png'},
                    {title : "Music files", extensions : 'mp3,ogg,wav,wma'},
                ],

                // Flash settings
                flash_swf_url : 'components/com_jbmslideshow/js/plupload/js/plupload.flash.swf',

                // Silverlight settings
                silverlight_xap_url : 'components/com_jbmslideshow/js/plupload/js/plupload.silverlight.xap'
            });

        var methods = {
            gc : {
                collection : $(),
                add : function(el) { methods.gc.collection = methods.gc.collection.add(el)}
            },
            xhrPool : [],
            _request : function(task, args, handle) {
                return $.ajax({
                    url: 'index.php?option=com_jbmslideshow&view=slideshow',
                    type: 'post',
                    dataType: 'json',
                    data: { task: task, args: args },
                    success: function(data) { handle(data); },
                    error: function(result) { log.error(result.responseText);  },
                    beforeSend: function(jqXHR) { methods.xhrPool.push(jqXHR) }
                });
            },
            init : function( options ) { 
                return this.each(function() {
                    var $this = $(this),
                        settings = {
                            autosave : false,
                            autosave_interval : 3, /* in minutes, minimum is 1 min */
                            slideControlsClass : 'slide-controls',
                            slideControlsSelector : '.slide-controls',
                            slideContentClass : 'slide-content',
                            slideContentSelector : '.slide-content',
                            toolbarSelector : '.toolbar',
                            toolbarClass : 'toolbar',
                            toolbarButtonsSelector : '.toolbar .buttons',
                            toolsSelector : '.toolbar .tools',
                            toolsClass : 'tools'
                        }
                    
                    // plugin not yet initialized?
                    if (typeof $this.data('jbmslideshowManager') == 'undefined') {   
                        $this.data('jbmslideshowManager', $.extend(settings, options));
                        o = $.extend($this.data('jbmslideshow'), $this.data('jbmslideshowManager'));
                        methods.$this = $this;
                        API = o.API;
                        methods.API = API;
                        $.extend(API, {
                            addSlide : methods.addSlide,
                            dialog : methods.dialog,
                            browse : methods.Browser.browse,
                            updateSlide : methods.updateSlide,
                            popup : methods.popup,
                            getAllSlides : methods.getAllSlides,
                            removeSlides : methods.removeSlides,
                            create : { 
                                icon : methods.create.icon,
                                button : methods.create.button,
                                checkbox : methods.create.checkbox,
                                selectlist : methods.create.selectlist,
                                custom : methods.create.custom
                            }
                        });
                    } else {
                        log.info('manager already initialized');
                        return;
                    }

                    // TODO check compatibility
                    // check JSON.stringify()
                    // check parseJSON
                    
                    // some additional options
                    o.contentWidth = $(o.contentSelector, $this).width();
                    o.logged = true;
                    
                    // preserve dimensions for preview changes
                    API.set('initial_width', API.get('width', 600, 'int')); /* preserve initial width */
                    API.set('initial_height', API.get('height', 400, 'int')); /* preserve initial height */
                    API.set('preview_height', (API.get('initial_height') / API.get('initial_width') * API.get('preview_width', 200, 'int')));
                    API.set('width', API.get('preview_width'));
                    API.set('height', API.get('preview_height'));
                    
                    // initialize manager dialog box
                    methods.dialog.init();
                    
                    $(o.contentSelector, $this).sortable({
                        placeholder:'placeholder',
                        stop: methods._reset
                        });
                    
                    /* prepare tools and settings */
                    var $tools = $(API.get('toolsSelector'), $this);
                    
                    $(o.contentSelector, $this)
                        .addClass('manager')
                        .after(function() { 
                                var b = $('<div class="add-slide-buttons"/>').append($('<span class="add-item-buttons-list" />'));
                                methods.gc.add(b);
                                return b;
                            }
                        );
                    
                    // add preview, save, delete all buttons 
                    var $previewB = methods.create.icon.call(methods, 'toolbar', {
                        title: 'preview',
                        clickFn : function(button) {     
                            var API = this.getAPI();
                            $(button).hide(); $previewStopB.show();
                            methods.gc.collection.hide('slow');
                            API.set('width', API.get('initial_width'));
                            API.set('height', API.get('initial_height'));
                            $(o.slideSelector, $this).css({
                                width: API.get('width'),
                                height: 'auto'
                            });
                            methods.updateSlides();
                            $this.trigger('jbmslideshow.onPreviewStart', API);
                        },
                        gc : false,
                        path : 'images/preview.png'
                    });
                    var $previewStopB = methods.create.icon.call(methods, 'toolbar', {
                        title: 'stop preview',
                        clickFn : function(button){
                            var API = this.getAPI();
                            $(button).hide(); $previewB.show();
                            methods.gc.collection.show('slow');
                            API.set('width', API.get('preview_width'));
                            API.set('height', API.get('preview_height'));
                            $(o.slideSelector, $this).css({
                                width: API.get('width'),
                                height: 'auto'
                            });
                            $this.trigger('jbmslideshow.onPreviewStop', API);
                            methods.updateSlides();
                            $(API.get('slideControlsSelector'), $this).css('display','');
                        },
                        gc : false, 
                        path : 'images/stop.png'
                    }).hide();
                    methods.create.icon.call(methods, 'toolbar', {
                        title: 'save',
                        clickFn : function(button) { 
                            methods._save();
                        },
                        path : 'images/save.png'
                    });
                    methods.create.icon.call(methods, 'toolbar', {
                        title: 'delete all',
                        clickFn : function(button) { 
                            methods._delete(methods.getAllSlides(), true, 'This will delete all slides, everything. Continue?');
                        },
                        path : 'images/trash.png'
                    });
                    /* TODO: add settings button */

                    /* prepare slides for editing */
                    $(o.slideSelector, $this).each(function(){
                        var extName = API.getSlideExtensionName(this),
                            $slide = $(this),
                            data = API.getExtensionData($slide, extName);
                        
                        $slide
                            .wrapInner($('<div >').addClass(API.get('slideContentClass')))
                            .append($('<div />').addClass(API.get('slideControlsClass')));
                        if (data) {
                            API.ExtensionManager.trigger(extName, 'update', [$slide, data]);
                        }
                        // add delete button
                        methods.create.icon.call(methods, $slide, {
                            title: 'delete slide', 
                            clickFn: function() { methods._delete($slide,true) }, 
                            path: 'images/delete2.png' }
                        );
                    });
                    
                    /* form buttons, tools etc. for each extension */
                    $.each(o.extensions, function(name, ext) {
                        
                        // add insert button for a plugin
                        if (ext.type == 'plugin') {
                            methods.addPluginButton(ext);
                        }
                        
                        // add slide controls of plugins and extensions
                        if ($.isPlainObject(ext.slideControls)) {
                            $.each(ext.slideControls, function() {
                                var control = this;
                                // insert plugin and extension controls
                                methods.getAllSlides().each(function() {
                                    if (ext.type == 'extension') {
                                        if (methods.create[control.type]) methods.create[control.type].call(ext, $(this), control);
                                    }
                                    if (API.getSlideExtensionName($(this)) == ext.name) {
                                        if (methods.create[control.type]) methods.create[control.type].call(ext, $(this), control);
                                    }
                                });
                            });
                        }
                        
                        // add toolbar controls
                        if (typeof ext.toolbarControls === 'object') {
                            $.each(ext.toolbarControls, function() {
                                if (methods.create[this.type]) methods.create[this.type].call(ext, 'toolbar', this);
                            });
                        }
                        
                        // add tools controls
                        if (typeof ext.toolsControls === 'object') {
                            $.each(ext.toolsControls, function() {
                                if (methods.create[this.type]) methods.create[this.type].call(ext, 'tools', this);
                            });
                        }                        

                    });
                    
                    // set autosave
                    if (API.get('autosave',true, 'boolean')) {
                        var i = 60000, autosave; 
                        if ((API.get('autosave_interval',1,'int') * 60000) < 1) {
                            log.info('Autosave interval incorrect. Setting it to 1 minute.');
                        } else { 
                            i = API.get('autosave_interval',1,'int') * 60000;
                        }
                        autosave = setInterval(methods._save, i);
                    }
                    
                    // run onReady from extensions
                    API.ExtensionManager.triggerEvent('onReady', [methods.getAllSlides()]);
                    
                    methods._reset();
                });
            },
            getAPI : function() {
                return methods.API;
            },
            addPluginButton : function(plugin) { 
                if (!plugin.label) { 
                    log.error('Failed to create a button. Missing some parameters.');
                    return false; 
                }
                plugin.label = 'add ' + plugin.label;
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings(),
                    $buttons = $('.add-item-buttons-list', $this);
                
                $buttons.append(
                    $('<a href="#" class="button" />')
                        .text(plugin.label)
                        .click(function(e) { 
                            e.preventDefault(); 
                            if (plugin.onInsert) plugin.onInsert.call(plugin);
                        })
                    );
            },
            addSlide : function(ext, callback) {
                if (!ext && ext.type != 'plugin') { log.error('Cannot add slide. No plugin specified.'); return; }
                
                // load layout
                methods.getLayout(ext.name, function($layout) {
                    var $this = methods.$this,
                        API = methods.getAPI(),
                        o = API.getSettings(),
                        $content = $(o.contentSelector, $this),
                        $newSlide = $('<div />')
                            .addClass(o.slideClass)
                            .addClass(ext.name)
                            .css({
                                width: API.get('preview_width',300)
                            })
                            .append($('<div />').addClass(API.get('slideContentClass')))
                            .append($('<div />').addClass(API.get('slideControlsClass')))
                            .appendTo($content);

                    API.setSlideDefaultData($newSlide, ext);
                    
                    // add delete button
                    methods.create.icon.call(methods, $newSlide, {
                        title: 'delete slide', 
                        path: 'images/delete2.png', 
                        clickFn: function(button) { methods._delete($newSlide, true) } 
                    });
                    
                    // add extension's slide controls
                    $.each(ext.slideControls, function() {
                        var control = this;
                        methods.create[control.type].call(ext, $newSlide, control);
                    });
                    $newSlide.find(o.slideContentSelector).append($layout);
                    API.ExtensionManager.triggerEvent('onAddSlide', [$newSlide]);
                    if ($.isFunction(callback)) callback.call(ext, $newSlide);
                });
            },
            getAllSlides : function(extension) {
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings();
                
                $slides = $(o.slideSelector, $this);
                // filter
                if (extension) { $slides = $slides.filter(function() { return API.getSlideExtensionName(this) == extension }) }
                return $slides;
            },
            updateSlide : function($slide, data) { 
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings(),
                    $slideContent = ($slide instanceof $) ? $slide.find(API.get('slideContentSelector'), $this) : $();
                
                if (!$slide.hasClass(o.slideClass) && $slide.parents(o.slideSelector).length == 1) {
                    $slide = $slide.parents(o.slideSelector, $this);
                    $slideContent = $slide.find(API.get('slideContentSelector'), $this);
                }
                
                var slideExt = API.getSlideExtensionName($slide),
                    slideData = API.getExtensionData($slide,slideExt);        
                
                // update slide's data
                if ($.isPlainObject(data)) {
                    $.extend(slideData, data);
                    // update slide by its extension
                    API.ExtensionManager.trigger(slideExt, 'update', [$slideContent, slideData]);
                }
            },
            updateSlides : function() {
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings();
                
                $(o.slideSelector, $this).each(function() {
                    methods.updateSlide($(this), API.getExtensionData($(this), API.getSlideExtensionName($(this))));
                });
            },
            getLayout : function(layout, callback) {
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings();

                methods._request('getLayout', {layout: layout}, function(r) {
                    if (r.error) {
                        m = r.error.message ? r.error.message : ''
                        log.error('Unable to retrieve the layout. ' + m);
                    } else {
                        if (callback) callback($(r.result.html));
                    }
                });
            },
            Browser : {
                frame : $('<div class="jbmslideshow-file-browser" />'),
                settings : {
                    label : 'File browser',
                    width : 100,
                    multipleSelect : false,
                    selectFn : function($item, data) {},
                    buttons : {},
                    controls : true,
                    filter : '.jpg|.jpeg|.png|.gif|.JPG|.JPEG|.PNG|.GIF'
                },
                browse : function(options) { 
                    var $this = methods.$this,
                        _this = this,
                        $browser = methods.Browser.frame.empty(),
                        API = methods.getAPI(),
                        o = API.getSettings(),
                        settings = $.extend(methods.Browser.settings, options),
                        MediaManager;
                        
                        // select functions
                        if (!window.JbmslideshowMediaManager) {
                            MediaManager = window['JbmslideshowMediaManager'] = {
                                init : function(folder) {
                                    this.setFolder(folder);
                                    this.selected = {};
                                },
                                currentFolder : '',
                                onFileSelected : function(filename, filepath) {
                                    this._setData(filename, filepath);
                                    settings.selectFn(this.getSelected()); 
                                    methods.dialog.close(); 
                                },
                                _setData : function(filename, filepath) {
                                    var _this = this;
                                    this.selected[filepath] = {
                                            filename: filename,
                                            filepath: filepath,
                                            folder: _this.getFolder()
                                        };
                                },
                                setFolder : function(folder) {
                                    this.currentFolder = folder;
                                    settings.folder = folder;
                                },
                                getFolder : function() {
                                    return this.currentFolder;
                                },
                                getSelected : function() {
                                    return this.selected;
                                },
                                onMultipleFilesSelected : function(filename, filepath) {
                                    var _this = this;
                                    if (!this.selected[filepath]) {
                                        _this._setData(filename, filepath);
                                    } else {
                                        delete this.selected[filepath];
                                    }
                                }
                            }
                        } else MediaManager = window['JbmslideshowMediaManager'];

                        var $mediaManager = $('<iframe/>')
                                .attr('id','folderframe')
                                .css({width: '100%', height:'80%'})
                                .attr('src','index.php?option=com_jbmslideshow&view=mediaList&tmpl=component&layout=details&folder=')
                        
                        if (settings.multipleSelect) {
                            settings.buttons.insertAll = {
                                label : 'insert all selected',
                                fn : function() { 
                                    if (Object.keys(MediaManager.getSelected()).length > 0) settings.selectFn.call(_this, MediaManager.getSelected()); 
                                    methods.dialog.close.call(_this); 
                                }
                            }
                        }
                        methods.dialog.open({
                            width : 80,
                            height : 80,
                            label : settings.label,
                            uploadButton : false,
                            buttons : settings.buttons,
                            uploadDir : settings.folder,
                            content : $browser.append($mediaManager)
                        });
                }
            },
            // deprecated..use Browser.browse()
            browse : function(options) {    
                var $this = methods.$this,
                    _this = this,
                    $browser = $('<div class="jbmslideshow-file-browser" />').append($('<div class="filelist" />')),
                    $list = $('.filelist', $browser),
                    API = methods.getAPI(),
                    o = API.getSettings(),
                    $selected = $(), selecteddata = new Array(),
                    settings = $.extend({}, {
                        label : 'File browser',
                        width : 100,
                        multipleSelect : false,
                        selectFn : function($item, data) {},
                        buttons : {},
                        mapfn : function(item) { return $('<img />').attr('src', API.getThumbSrc({file: item.filename, folder: item.folder, size:[300,300]}) /*API.get('mediaUrl') + '/' + item.folder + '/' +item.filename*/) },
                        folder : 'images',
                        controls : true,
                        filter : '.jpg|.jpeg|.png|.gif|.JPG|.JPEG|.PNG|.GIF'
                    }, typeof options === 'object' ? options : (o.browser_settings ? o.browser_settings : {}));
                    
                //save browser settings
                o.browser_settings = settings;
                
                methods._request('getFileList', {folder: settings.folder, filter: settings.filter}, function(r) {
                    if (r.error) {
                        var m = r.error.message ? r.error.message : '';
                        log.error('Error retrieving file list: ' + m);
                    } else if (r.result) {
                        if (r.result.length < 1) { $list.append($('<span/>').text('No files found...')); }
                        $.each(r.result, function() {
                            var item = this,
                                $item = settings.mapfn(item)
                                    .click(function() { 
                                        settings.selectFn($item, $.extend(true, {}, item)); 
                                        methods.dialog.close(); 
                                    });
                                API.setData($item, item);
                            
                            $itemContainer = $item.wrap('<div class="item-content" />').parent().wrap('<div class="item-wrapper"/>').parent();

                            if (settings.controls) {
                                if (settings.multipleSelect) {
                                    methods.create.checkbox($itemContainer, {    
                                        name: 'selectedFiles',
                                        clickFn: function(control) {
                                            if ($(control).is(':checked')) {
                                                $selected = $selected.add($item);
                                                selecteddata.push(item);
                                            } else {
                                                $selected = $selected.not($item);
                                                selecteddata.splice($.inArray(item, selecteddata),1);
                                            }
                                        }
                                    });
                                }
                                methods.create.icon($itemContainer, {   
                                    title : 'delete file',
                                    path : 'images/trash.png', 
                                    clickFn: function(control) {
                                        var r = false, 
                                            path = API.get('mediaPath') + API.get('DS') + item.folder + API.get('DS') + item.filename,
                                            $button = $(control);
                                        r = confirm('Are you sure you want to delete the file?');
                                        if (r) {
                                            methods._request('deleteFiles', [path], function(r) {
                                                if (r.error) { 
                                                    log.error('Failed to delete file at path: ' + path);
                                                } else { 
                                                    $button.parents('.item-wrapper').hide('slow', function() {$(this).remove()});
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                            $list.append($itemContainer);
                        });
                    }
                    if (settings.multipleSelect) {
                        settings.buttons.insertAll = {
                            label : 'insert all selected',
                            fn : function() { 
                                settings.selectFn.call(_this, $selected, selecteddata); 
                                methods.dialog.close.call(_this); 
                            }
                        }
                    }
                    methods.dialog.open({
                        width : settings.width,
                        label : settings.label,
                        uploadButton : true,
                        buttons : settings.buttons,
                        uploadDir : settings.folder,
                        content : $browser
                    });
                });
            },
            _update : function() {
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings(),
                    $content = $(o.contentSelector, $this),
                    list = {};
                
                // update list of slides data
                $content.find(o.slideSelector).each(function(i, slide) {
                    list[i] = API.getData(slide);
                });
                o.list = list;
            },
            _save : function() {
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings(),
                    Itemid = API.get('Itemid'),
                    id = API.get('id');
                methods._reset();
                methods._update();

                methods._request('save', { content: o.list, id: id, Itemid: Itemid }, function(r) {
                    if (r.result && r.result.saved) {
                        methods.popup('Everything saved');
                    } else {
                        var errmsg = '';
                        if (r.error) { var errmsg = r.error.message; }
                        log.error('Failed to save: ' + errmsg);
                        methods.popup('Saving failed, please try it again.', 'error');
                        return;
                    } 
                });
            },
            /* 
            * function to show popup message
            * @text ... text to show in popup
            * @type ... class name, can be success or error
            */
            popup : function(text, type) {
                if (!type) type = 'success';
                $('<div id="popup" class="popup-message ui-corner-all '+ type +'" />')
                    .html(text)
                    .hide()
                    .prependTo($('body'))
                    .fadeIn('slow', function(){ $(this).delay(3000).fadeOut('slow', function() { $(this).remove() }) });
            },
            _delete : function($items, ask, msg) {
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings(),
                    r = false,
                    ask = ask ? true : false;
                    msg = msg ? msg : 'Are you sure you want to delete the item(s)?';
                r = ask ? confirm(msg) : true;
                if (r) {
                    $items.each(function() {
                        $(this).hide('slow', function() { $(this).remove() } );
                    });
                    methods._reset();
                }
            },
            removeSlides : function($slide) {
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings();
                                    
                $slide.each(function() {
                    var $s = $(this);
                    if ($s.parents(o.slideSelector).length) { $s = $s.parents(o.slideSelector) }
                    $s.hide('slow', function() { $s.remove() } );
                });
            },
            _reset : function() {
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings();
                
                // close dialog
                methods.dialog.close();
                
                // remove empty slides
                $(o.slideSelector,$this).filter(function(){ return !$(o.slideContentSelector,this).html() }).remove();
                
                // run refresh methods from extensions
                API.ExtensionManager.triggerEvent('refresh');
                
                // reset z-index                
                var count = $(o.slideSelector, $this).length;
                $(o.slideSelector, $this).each(function(i,el){
                    $(el).css('z-index',count-i);
                })
            },
            create : {
                icon : function(target, params) {
                    var $this = methods.$this,
                        _this = this, /* context depending on where it is called from */
                        API = methods.getAPI(),
                        defaults = {
                            title: '',
                            clickFn : function(){},
                            attr : {},
                            gc : true, 
                            path : ''
                        },
                        $target = methods.create._checkTarget(target);
                        
                    params = $.extend({}, defaults, params);
                    
                    var $icon = $('<a />')
                        .addClass('icon')
                        .attr($.isPlainObject(params.attr) ? params.attr : {})
                        .attr({
                                'href' : '#',
                                'title' : params.title
                            })
                        .css('background-image', params.path ? 'url(' + API.get('pluginUrl') +'/'+ params.path + ')' : 'none')
                        .click(function(e){ e.preventDefault(); methods.create._runClickFn.call(_this, params.clickFn, this) })
                    $target.append($icon);
                    if (params.gc) methods.gc.add($icon);
                    return $icon;
                },
                checkbox : function(target, params) {
                    var $this = methods.$this,
                        _this = this,
                        API = methods.getAPI(),
                        defaults = {
                            name: 'check',
                            clickFn : function(){},
                            attr: {},
                            gc: true
                        },
                        $target = methods.create._checkTarget(target);
                    
                    params = $.extend({}, defaults, params);
                    
                    var $checkbox = $('<input type="checkbox">')
                        .addClass('checkbox')
                        .attr($.isPlainObject(params.attr) ? params.attr : {})
                        .attr('name', params.name)
                        .click(function() {
                            methods.create._runClickFn.call(_this, params.clickFn, this) 
                        })
                    $target.append($checkbox);
                    if (params.gc) methods.gc.add($checkbox);
                    return $checkbox;
                },
                button : function(target, params) {
                    var $this = methods.$this,
                        _this = this, /* context depending on where it is called from */
                        API = methods.getAPI(),
                        defaults = {
                            label: '',
                            clickFn : function(){},
                            attr : {},
                            gc : true
                        },
                        $target = methods.create._checkTarget(target);
                    
                    // check params
                    if (typeof params.label != 'string') { 
                        log.error('A button was not created. Label was not specified.');
                        return;
                    } else 
                        params = $.extend({}, defaults, params);
                    
                    var $button = $('<a />')
                        .addClass('button')
                        .attr($.isPlainObject(params.attr) ? params.attr : {})
                        .attr({
                                'href' : '#',
                                'title' : params.title
                            })
                        .text(params.label)
                        .click(function(e){ e.preventDefault(); methods.create._runClickFn.call(_this, params.clickFn, this) })
                    $target.append($button);
                    if (params.gc) methods.gc.add($button);
                    return $button;
                },
                selectlist : function(target, params) {
                    var $this = methods.$this,
                        _this = this, /* context depending on where it is called from */
                        API = methods.getAPI(),
                        defaults = {
                            update : false,
                            list : new Array(),
                            onSetItem : function(){},
                            onItemClicked : function() {},
                            onAddItem : function(){},
                            onDeleteItem : function(){},
                            onEditItem : function(){},
                            onSorted : function(){},
                            sortable : false,
                            modifiable : false,
                            attr : {},
                            showHead : true,
                            collapsible : true,
                            disableCurrent : true,
                            current : null,
                            initialHead : 'edit items',
                            gc : true
                        },
                        $target = methods.create._checkTarget(target);
                    
                    // check params                    
                    if (!$.isArray(params.list)) { 
                        log.error('A selectlist button was not created. List is not an Array or a current item was not specified.');
                        return;
                    } else 
                        params = $.extend({}, defaults, params);
                     
                    var list = params.list,
                        $head = $('<div />').addClass('selectlist-head'),
                        $ul = $('<ul />').addClass('selectlist-list'),
                        mod = params.modifiable;
                    
                    var setHead = function(value) {
                        $head.text(value).attr('value', value);
                        if (!params.showHead) $head.hide();
                    }
                    
                    var getList = function($ul) {
                        var list = new Array();
                        $ul.find('a[value]').each(function() {
                            list.push($(this).attr('value'));
                        })
                        return list;
                    }
                    
                    if (params.sortable) $ul.sortable({
                        axis:'y',
                        items:'> li',
                        stop:function(e,ui){ params.onSorted.call(_this,getList($(ui.item).parents('ul'))) }
                    });
                    
                    // set initial head or current item
                    if (!list.length) {
                        setHead(params.initialHead);
                    } else { 
                        setHead(params.current);
                        params.onSetItem.call(_this, params.current);
                    }

                    var deleteItem = function(val, $ul) {
                        $ul.find('*[value="'+val+'"]').parents('li').remove();
                        
                        if (!$ul.find('li').length) {
                            setHead(params.initialHead);
                        } else {
                            var v = $ul.find('li:first *[value]').attr('value');
                            setHead(v);
                            params.onSetItem.call(_this, v);
                        }
                        return $ul;
                    }
                    
                    var itemExists = function(val, $ul) {
                        return $ul.find('*[value="'+val+'"]').length > 1;
                    }
                    
                    var editItem = function(val, $ul, $button) {
                        var $item = $ul.find('*[value="'+val+'"]').hide(),
                            $input = $('<input type="text" size="20" />')
                                        .val(val)
                                        .addClass('selectlist-edititem-input')
                                        .attr('placeholder','insert text')
                                        .onEnter(function(e){
                                            var newVal = $.trim($(this).val());
                                            if (!newVal) { 
                                                $(this).remove();
                                                $item.show();
                                                return;
                                            }
                                            if (itemExists(newVal, $ul)) { alert('Item already exists'); return; }
                                            
                                            // if we changed current item, change it in head too and set current item
                                            if ($head.attr('value') == $item.attr('value')) setHead(newVal);
                                            params.onSetItem.call(_this, newVal);
                                            
                                            params.onEditItem.call(_this, $item.attr('value'), newVal);
                                            $item.attr('value', newVal).text(newVal);
                                            $button.show();
                                            $(this).remove();
                                            $item.show();
                                        })
                        $item.after($input); $input.focus();
                    }
                    
                    var addItem = function(val, $ul) {
                        if (!val) return;
                        var $item = $('<a />')
                                        .addClass('item-label')
                                        .text(val)
                                        .attr({
                                            'href' : '#',
                                            'value' : val
                                        })
                                        .click(function(e){
                                            e.preventDefault();
                                            $(this).parents('.selectlist-wrapper').hide('fast');
                                            params.onItemClicked.call(_this, $(this).attr('value'));
                                            //params.onSetItem.call(_this, $(this).attr('value'));
                                            setHead($(this).attr('value'));
                                        })

                        var $itemCont = $('<li />').addClass('item').append($item);
                                
                        $ul.append($itemCont);
                        if (mod) {
                            $itemCont
                             .append($('<a />')
                                .addClass('selectlist-edit-button')
                                .text('edit')
                                .attr('href','#')
                                .click(function(e){
                                    e.preventDefault();
                                    $(this).hide();
                                    editItem($item.attr('value'), $ul, $(this)) 
                                })
                            )
                            .append($('<a />')
                                .addClass('selectlist-delete-button')
                                .text('delete')
                                .attr('href','#')
                                .click(function(e){
                                    e.preventDefault();
                                    if (confirm('All slides in the category will be deleted too. Continue?')) {
                                        deleteItem($item.attr('value'), $ul)
                                        params.onDeleteItem.call(_this, $item.attr('value')); 
                                    }
                                })
                            )
                        }
                        // if first item, set it in head
                        if ($ul.find('> li').length == 1) {
                            setHead(val);
                            params.onSetItem.call(_this, val);
                        }
                        return $ul;
                    }
                    
                    $.map(list, function(val, key){
                        if (!val) return;
                        $ul = addItem(val, $ul);
                    });
                    
                    if (mod) {
                        var $input = $('<input type="text" size="20" />')
                                        .addClass('selectlist-additem-input')
                                        .attr('placeholder','add item')
                                        .onEnter(function(e){
                                            if (itemExists($(this).val(), $ul)) { alert('Item already exists'); return; }
                                            addItem($(this).val(), $ul); 
                                            params.onAddItem.call(_this, $(this).val());
                                            $(this).val('');
                                        })
                        var $inputCont = $('<div />')
                            .addClass('selectlist-additem-input-container')
                            .append($input)
                            .append($('<a />')
                                .addClass('selectlist-add-button')
                                .text('+')
                                .attr('href','#')
                                .click(function(e){
                                    e.preventDefault();
                                    if (itemExists($input.val(), $ul)) { alert('Item already exists'); return; }
                                    addItem($input.val(), $ul);
                                    params.onAddItem.call(_this, $input.val());
                                    $input.val('');
                                })
                            )
                    }
                    var $selectlistWrapper = $('<div />').addClass('selectlist-wrapper')
                            .append($ul)
                            .append($inputCont)

                    var $selectlist = $('<div />')
                        .addClass('selectlist')
                        .attr($.isPlainObject(params.attr) ? params.attr : {})
                        .append(function() { 
                            if (params.collapsible) return $('<div />').addClass('selectlist-handle')
                                .click(function(e) {
                                    if ($selectlistWrapper.is(':hidden')) { $selectlistWrapper.show('slow'); }
                                    else { $selectlistWrapper.hide('fast') }
                                })
                            }
                        )
                        .append($head)
                        .append($selectlistWrapper)
                        
                    if (!mod) $selectlist.addClass('plainlist');
                    if (params.collapsible) { $selectlistWrapper.hide(); } 
                    //else $target.mouseleave(function(){$selectlist.remove()})
                    
                    $target.append($selectlist);
                    if (params.gc) methods.gc.add($selectlist);
                    return $selectlist;
                },
                custom : function(target, params) {
                    var $this = methods.$this,
                    _this = this, /* context depending on where it is called from */
                    API = methods.getAPI(),
                    defaults = {
                        clickFn : function(){},
                        attr : {},
                        gc : true
                    },
                    $target = methods.create._checkTarget(target);
                    
                    params = $.extend({}, defaults, params);
                    
                    var $custom = $('<div />')
                        .addClass('custom')
                        .attr($.isPlainObject(params.attr) ? params.attr : {})
                        .click(function(e){ e.preventDefault(); methods.create._runClickFn.call(_this, params.clickFn, this) })
                    $target.append($custom);
                    if (params.gc) methods.gc.add($custom);
                    return $custom;
                },
                _runClickFn : function(fn, clickedEl) {
                    if (typeof fn == 'function') {
                        var callbackEl = clickedEl;
                        // if we are in slide, return slide-content 
                        if ($(clickedEl).parents(API.get('slideSelector')).length) {
                            callbackEl = $(clickedEl).parents(API.get('slideSelector')).find(API.get('slideContentSelector'));
                        } 
                        fn.call(this, $(callbackEl), $(clickedEl));
                    }
                },
                _checkTarget : function(target) {
                    if (typeof target == 'string') {
                        switch (target) {
                            case 'toolbar' :
                                return $(API.get('toolbarButtonsSelector'));
                                break;
                            case 'tools' :
                                return $(API.get('toolsSelector'));
                                break;
                        }
                    } else if ($(target).hasClass(o.slideClass)) { 
                        return $(target).find(API.get('slideControlsSelector'));
                    } else if ($(target).parents(o.slideSelector).length) {
                        return $(target).parents(o.slideSelector).find(API.get('slideControlsSelector'));
                    } else return $(target)
                }
            },
            /* TODO: need to find out button idenfier
            buttonExists : function($el, label) {
                var $this = methods.$this,
                    API = methods.getAPI(),
                    o = API.getSettings();
                
                if (!$el.hasClass(o.slideClass) && $el.parents(o.slideSelector).length == 1) {
                    $el = $el.parents(o.slideSelector, $this);
                }
                
                return ($el.find('a:contains('+label+')').length);
            },
            */
            dialog : {
                init : function() {
                    // check if not already initialized 
                    if (this.$dialog) return;
                    
                    $('body')
                        .append($('<div id="jbmslideshow-dialog" />').hide()
                            .append($('<div id="jbmslideshow-dialog-background" />'))
                            .append($('<div id="jbmslideshow-dialog-container"/>')
                                    .append($('<div id="jbmslideshow-uploader"/>'))
                                    .append($('<div class="dialogHeading" />'))
                                    .append($('<div class="message" />'))
                                    .append($('<div class="content"><div class="wrapper" /></div>'))
                                    .append($('<div class="buttons" />'))
                            )
                        );
                    this.$dialog = $('#jbmslideshow-dialog');
                    this.$dialogContainer = $('#jbmslideshow-dialog-container');
                    this.$dialogLabel = $('#jbmslideshow-dialog .dialogHeading');
                    this.$dialogMessage = $('#jbmslideshow-dialog .message');
                    this.$dialogContent = $('#jbmslideshow-dialog .content .wrapper');
                    this.$dialogButtons = $('#jbmslideshow-dialog .buttons')
                                            .append($('<a href="#" id="jbmslideshow-upload-button" />')
                                                    .addClass('button')
                                                    .text('upload')
                                            );
                    this.$uploader = $('#jbmslideshow-uploader');
                    this.$uploadButton = $('#jbmslideshow-upload-button');
                    
                    uploader.init();
                    // bind uploader events
                    uploader.bind('beforeUpload', function(up, file) {
                        up.settings.multipart_params = {'upload_dir' : up.settings.upload_dir }
                    });
                    uploader.bind('FilesAdded', function(up, files) {
                        up.start();
                        up.refresh(); // Reposition Flash/Silverlight
                    });
                    uploader.bind('UploadProgress', function(up, file) {
                        methods.dialog.message(file.percent + "%");
                        up.refresh(); // Reposition Flash/Silverlight
                    });
                    uploader.bind('Error', function(up, err) {
                        methods.dialog.message('File ' + err.file.name + ": " + err.message);
                        log.error(err.file.name + ": " + err.message);
                        up.refresh(); // Reposition Flash/Silverlight
                    });
                    uploader.bind('UploadComplete', function(up, files) {
                        methods.dialog.message('Upload complete');
                        //methods.browse();
                        methods.Browser.add(files);
                        up.refresh(); // Reposition Flash/Silverlight
                    });
                    uploader.bind('FileUploaded', function(up, file, response) {
                        try {
                            var response = jQuery.parseJSON(response.response);
                            if(response.error) {
                                methods.dialog.message('File "' + file.name + '" could not be uploaded. Try it again.');
                                log.error('Upload failed: ' + file.name + ':' + response.error.message);
                            }
                        }
                        catch (err) {
                            log.error('Upload failed: ' + file.name + ': ' + err);
                        }
                    });
                    
                },
                message : function (msg) {
                    if (typeof msg !== 'string') return;
                    this.$dialogMessage
                            .stop()
                            .empty()
                            .text(msg)
                            .show()
                            .delay(3000)
                            .hide('slow', function() {this.empty()});
                },
                open : function(options) { 
                    var context = this,
                        _this = methods.dialog, left, s = {},
                        defaults = {
                            width: 100,
                            height: 50,
                            label: '',
                            overflow: true,
                            buttons: { cancel : _this.close },
                            uploadButton: false,
                            uploadDir: 'images',
                            content: $('<div />'),
                            callbackEl: $('<div />')
                        };
                    s = $.extend(true, {}, defaults, options);
                    if (_this.isOpen()) { _this.$dialogContent.hide('fast').empty().append(s.content).show('fast'); return }
                    
                    // set dimensions
                    s.width = parseInt(s.width) < 90 ? $('body').width() * parseInt(s.width) / 100 : $('body').width() * 0.9;
                    s.height = parseInt(s.height) < 90 ? $(window).height() * parseInt(s.height) / 100 : $(window).height() * 0.8;
                    // set limits
                    s.width = s.width < 200 ? 200 : s.width;
                    s.width = s.width > 900 ? 900 : s.width;
                    
                    left = Math.floor((($('body').width() - s.width) / 2) / $('body').width() * 100);
                    if (s.uploadButton) {
                        _this.$uploader.show();
                        _this.$uploadButton.show();
                        if (typeof s.uploadDir === 'string') uploader.settings.upload_dir = s.uploadDir;
                    } else {
                        _this.$uploader.hide();
                        _this.$uploadButton.hide();
                    }
                    
                    _this.$dialog.attr('class','').addClass(context.name ? context.name : '')
                    _this.$dialogContainer.css({width: s.width, left: left + '%', height: s.height});
                    _this.$dialogLabel.empty().text(s.label);
                    _this.$dialogMessage.empty().append(s.message);
                    _this.$dialogContent.empty().append(s.content);
                    _this.$dialogContent.css({overflow: s.overflow ? 'auto' : 'hidden', maxHeight: s.height});
                    
                    $.each(s.buttons, function(label, settings) {
                        var fn = function(){}, attr = '';
                        if (typeof settings === 'function') fn = settings;
                        if (typeof settings === 'object') {  
                            fn = settings.fn ? settings.fn : fn;
                            label = settings.label ? settings.label : label;
                            attr = settings.attr ? settings.attr : attr;
                        }

                        methods.create.button.call(context, _this.$dialogButtons, {
                            label : label,
                            attr : attr,
                            clickFn : fn
                        });
                    });
                    if (s.uploadButton) uploader.refresh(); 
                    _this.$dialog.fadeIn();
                },
                close : function() {
                    var _this = methods.dialog;
                    _this.$dialog
                        .fadeOut()
                        .attr('class','')
                        .toggleClass(this.name ? this.name : '')
                    _this.$dialogLabel.empty();
                    _this.$dialogContent.empty();
                    _this.$dialogButtons.children().not($('#jbmslideshow-upload-button')).remove();
                },
                isOpen : function() {
                    var _this = methods.dialog;
                    return _this.$dialog.is(':hidden') ? false : true;
                }
            }
        }

        $.fn.jbmslideshowManager = function( method ) {
            var public = 'destroy',
                isPublic = false;
            
            if (typeof method === 'string') {
                var r = new RegExp(method, 'i');
                isPublic = r.test(public);
            }
            
            // Method calling logic
            if (typeof method ==='string' && !isPublic) {
                log.error( method + ' is a protected method on jQuery.jbmslideshowManager' );
            } else if ( methods[method] ) {
                return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply( this, arguments );
            } else {
                log.error( 'Method ' +  method + ' does not exist on jQuery.jbmslideshowManager' );
            } 
        };
        
	});
} (this.jQuery.noConflict(), this, this.document));