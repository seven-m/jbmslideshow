<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
$user = JFactory::getUser();
$params = new JRegistry;
$dispatcher	= JDispatcher::getInstance();
$dispatcher->trigger('onContentBeforeDisplay', array('com_jbmslideshow.file', &$this->_tmp_img, &$params));
?>
		<tr>
			<td>
				<a class="img-preview" href="<?php echo COM_JBMSLIDESHOW_MEDIA_URL.'/'.$this->_tmp_img->path_relative; ?>" title="<?php echo $this->_tmp_img->name; ?>"><?php echo JHtml::_('image', COM_JBMSLIDESHOW_MEDIA_URL.'/'.$this->_tmp_img->path_relative, JText::sprintf('COM_MEDIA_IMAGE_TITLE', $this->_tmp_img->title, MediaHelper::parseSize($this->_tmp_img->size)), array('width' => $this->_tmp_img->width_16, 'height' => $this->_tmp_img->height_16)); ?></a>
			</td>
			<td class="description">
				<a href="javascript:window.parent.JbmslideshowMediaManager.onFileSelected('<?php echo $this->_tmp_img->name.'\',\''.$this->_tmp_img->path_relative.'\''; ?>)" title="<?php echo $this->_tmp_img->name; ?>" rel="preview"><?php echo $this->escape($this->_tmp_img->title); ?></a>
			</td>
			<td>
				<?php echo JText::sprintf('COM_MEDIA_IMAGE_DIMENSIONS', $this->_tmp_img->width, $this->_tmp_img->height); ?>
			</td>
			<td class="filesize">
				<?php echo MediaHelper::parseSize($this->_tmp_img->size); ?>
			</td>
			<td>
				<input type="checkbox" name="selection[]" onclick="javascript:window.parent.JbmslideshowMediaManager.onMultipleFilesSelected('<?php echo $this->_tmp_img->name.'\',\''.$this->_tmp_img->path_relative.'\''; ?>)" value="<?php echo $this->_tmp_img->name; ?>" />
			</td>
		</tr>
<?php
$dispatcher->trigger('onContentAfterDisplay', array('com_jbmslideshow.file', &$this->_tmp_img, &$params));
?>
