(function ($, window, document) {
	$(function () {
        
        // start the slideshow
        var play = function(API) {
            var $this = $(this),
                o = API.getSettings(),
                $container = $('#content-container');
            
            // adjust container padding
            var adjustPadding = function() {
                return ($(window).width() - $container.width())/2;
            }
            $container.css('padding','0 ' + adjustPadding() + 'px');
            $(window).resize(function() { $container.css('padding','0 ' + adjustPadding() + 'px'); });
            
            $(o.contentSelector, $this).find('.slide-controls').hide();
            
            $(o.contentSelector, $this)
                .clone(true)
                .attr('id','jbmslideshow-show')
                .removeAttr('class')
                .addClass('loading')
                .insertAfter($(o.contentSelector, $this))
                .find('.slide-controls').hide()
                .end().end().hide();
            
            $show = $('#jbmslideshow-show');
            $('.plugin-image img', $show).css('height', API.get('height',400));
            
            $(window).ready(function() {
                $('.jbmslideshow-navi', $this)
                    .append($('<a href="#" class="prev"/>'))
                    .append($('<a href="#" class="next"/>'))
                    .delay(1000)
                    .fadeOut('slow');
                
                $('#jbmslideshow-show')
                    .removeClass('loading')
                    .on('cycle-initialized', function(e, oh) {
                        $(oh.slides).css('opacity',0.3);
                    })
                    .on('cycle-before', function(e, oH, outEl, inEl, forwFlag) {
                        $(outEl).animate({opacity: 0.3},API.get('speed', 500, 'int'));
                        $(inEl).animate({opacity: 1}, API.get('speed', 500, 'int'));
                    })
                    .cycle({
                        next : $('.jbmslideshow-navi .next', $this),
                        prev : $('.jbmslideshow-navi .prev', $this),
                        pauseOnHover : API.get('pause', true, 'boolean'),
                        speed : API.get('speed', 500, 'int'),
                        timeout : API.get('timeout', 4000, 'int'),
                        slides : '.slide',
                        log : false,
                        swipe : true,
                        fx : API.get('effect', 'fade')
                    })
                    .cycle('prev')
            })           
            
            $('.content-wrapper', $this)
                .mouseenter(function() {$('.jbmslideshow-navi', $this).fadeIn()})
                .mouseleave(function(){ $('.jbmslideshow-navi', $this).fadeOut()})
        }
        
        $('#jbmslideshow').bind('jbmslideshow.onStarted', function(event, API) {
            var _this = this;
            
            if (!$.fn.cycle) {
                $.getScript(API.get('pluginUrl') + "/libraries/jquery-cycle.js", function(data, textStatus, jqxhr)     
                    {
                        log.info('Jquery-cycle script loaded.');
                        play.call(_this, API);
                    });
            } else {
                play.call(_this, API);
            }
        });
    });
} (this.jQuery.noConflict(), this, this.document));        