<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 */
class JbmslideshowViewSquaregallery extends JbmslideshowView
{
    function __construct() 
    {        
        // load custom js
        $this->addCustomScript('jquery-cycle2.js');
        $this->addCustomScript('jquery-cycle2-swipe.js');
    }
}
