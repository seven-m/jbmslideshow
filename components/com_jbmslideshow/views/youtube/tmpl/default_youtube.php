<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2012 Jan B Mwesigwa, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
?>

<div class="plugin-youtube">
    <div class="wrapper">
        <div class="youtube-item" style="background-image:url(<?php echo $this->youtube->get('data.youtube.thumbimage'); ?>)">
            <div style="display:none;">
                <img src="<?php echo $this->youtube->get('data.youtube.thumbimage'); ?>" alt="<?php echo $this->youtube->get('data.youtube.title'); ?>" />
                <h2><?php echo $this->youtube->get('data.youtube.title'); ?></h2>
                <p><?php echo $this->youtube->get('data.youtube.description'); ?></p>
            </div>
        </div>
    </div>
</div>