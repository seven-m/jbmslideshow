(function ($, window, document) {
	$(function () {
        // Image Plugin API
        var jcrop_api,
            ImagePlugin = {
                label : "image",
                onInsert : function() { 
                    var API = this.getAPI(),
                        _this = this;
                        _this._selectImage.call(_this, null, true);
                },
                slideControls : {
                    changeImage : {
                        type: 'icon',
                        title: 'change image', 
                        clickFn: function($slide) { this._selectImage.call(this, $slide) },
                        path : 'extensions/image/images/changeimage.png'
                    },
                    crop : {
                        type: 'icon',
                        title: 'crop', 
                        clickFn: function($slide) { this._crop.call(this, $slide) },
                        path : 'extensions/image/images/crop.png'
                    },
                    changeDescription : {
                        type: 'icon',
                        title: 'description',
                        clickFn: function($slide) { this._changeDescription.call(this, $slide) },
                        path : 'extensions/image/images/description.png'
                    }
                },
                _selectImage : function($slide, multipleSelect) {
                    var _this = this,
                        API = this.getAPI();
                        
                    API.browse.call(_this, {
                        label : 'Select an image',
                        width : 90,
                        multipleSelect : multipleSelect ? true : false, 
                        /* selectFn runs when image in browser is clicked. 
                        *  if multipleSelect is true, @$imgs and @data are array 
                        */
                        buttons : {
                            cancel : function() { API.dialog.close.call(_this) }
                        },
                        selectFn: function(imgs){ 
                            $.each(imgs, function(index, img) {
                                var d = img;
                                
                                $.extend(d, {
                                    size : [API.get('width',600, 'int'), API.get('height',400, 'int')],
                                    crop : null
                                });
                                if (!$slide) {
                                    API.addSlide.call(_this, _this, function($newSlide) { 
                                        API.updateSlide($newSlide, d); 
                                    });
                                } else {
                                    API.updateSlide($slide, d);
                                }
                            });
                        } 
                    });
                },
                _changeDescription : function($slide) {
                    var _this = this,
                        API = this.getAPI(),
                        data = API.getExtensionData($slide, _this),
                        $content = $slide.clone()
                                    .append($('<span class="label">Description:</span><input type="text" class="field-description"/>'))
                                    .append($('<br />'))
                                    .append($('<span class="label">Font color:</span><select class="field-color" /><span class="label">Background color:</span><select class="field-background" /><span class="label">Font size:</span><select class="field-font-size" /><br />'))
                                    .append($('<span class="label">Position:</span><span class="label">top</span><input type="radio" name="field-position" value="top"/><span class="label">bottom</span><input type="radio" name="field-position" value="bottom"/>')); 
                    
                    // get values or fill with defaults
                    var desc = data.description ? data.description : {};
                    desc = $.extend({}, {fontSize: '100%', background: 'default', color: '', text: '', position: 'bottom'}, desc);

                    // adjust image and prepare controls
                    $content.find('.field-description').val(desc.text);
                    $content.find('img').css({width: API.get('initial_width', 600, 'int'), height: 'auto'});
                    $content.find('.field-description').change(function() {$content.find('.description').text($(this).val())});
                    $.each(['default','white','black','red','yellow','green','blue'], function() { 
                        $content.find('select.field-color').append($('<option/>').text(this).attr('value',this)); 
                        $content.find('select.field-background').append($('<option/>').text(this).attr('value',this)); 
                    }); 
                    $.each(['100%','130%','160%','180%','250%'], function() { 
                        $content.find('select.field-font-size').append($('<option/>').text(this).attr('value',this)); 
                    });
                    
                    $content.find('.field-color')
                        .change(function(){ 
                            if ($(this).val() == 'default') {
                                $content.find('.description').css('color', '');
                            } else {
                                $content.find('.description').css('color', $(this).val());
                            }
                        })
                        .val(desc.color).change();
                        
                    $content.find('.field-background')
                        .change(function(){ 
                            if ($(this).val() == 'default') {
                                $content.find('.background').css('background-color', '');
                            } else {
                                $content.find('.background').css('background-color', $(this).val());
                            }
                        })
                        .val(desc.background).change();
                        
                    $content.find('.field-font-size')
                        .change(function(){
                            $content.find('.description').css('font-size', $(this).val());
                        })
                        .val(desc.fontSize).change();
                        
                    $content.find('input:radio[name="field-position"]')
                        .change(function(){
                            if ($(this).val() == 'top') {
                                $content.find('.description-wrapper').css({'top': 0, 'bottom': 'auto'});
                            } else {
                                $content.find('.description-wrapper').css({'bottom': 0, 'top': 'auto'});
                            }
                        })
                        .filter('[value='+desc.position+']').attr('checked', true)
                        .change();

                    // push all into system dialog
                    API.dialog.open.call(_this, {
                        label: "Set image description",
                        width: API.get('initial_width', 600,'int') + 40,
                        height: API.get('initial_height', 400,'int') + 230,
                        buttons: {
                            save : function() { _this._saveDescription.call(_this, $slide); API.dialog.close.call(_this); },
                            cancel : function() { API.dialog.close.call(_this); }
                        },
                        content: $content
                    });
                    
                    // some additional controls setup
                    $content.find('.description-wrapper').css('width', $content.find('img').width());
                },
                _saveDescription : function($slide) {
                    var API = this.getAPI(),
                        desc;
                    
                    // collect values
                    desc = {
                        fontSize : $('.field-font-size', API.dialog.$dialog).val(),
                        background : $('.field-background', API.dialog.$dialog).val(),
                        color : $('.field-color', API.dialog.$dialog).val(),
                        text : $('.field-description', API.dialog.$dialog).val(),
                        position : $('input:radio[name="field-position"]:checked', API.dialog.$dialog).val(),
                    }
                    API.updateSlide($slide, { description: desc });
                },
                _crop : function($slide) {
                    var _this = this,
                        API = this.getAPI(),
                        data = API.getExtensionData($slide, _this),
                        $img = $slide.find('img'),
                        $cropimage = $('<img />').attr('src', API.get('mediaUrl') + '/' + data.folder + '/' + data.filename); 
                    
                    // open in edit window
                    API.dialog.open.call(_this, {
                        label : "Crop the image",
                        width: 420,
                        height: 420,
                        overflow: false,
                        buttons : {
                                save : function() { _this._saveCropped.call(_this, $slide); API.dialog.close.call(_this); },
                                cancel : function() { jcrop_api.destroy(); API.dialog.close.call(_this); }
                            },
                        content : $cropimage,
                        callbackEl : $slide
                    });
                    // start JCrop
                    // TODO: minSize, aspectRatio
                    $cropimage.Jcrop({
                            boxHeight: 400,
                            boxWidth: 400,
                            aspectRatio: API.get('width',600,'int') / API.get('height',400,'int'),
                            minSize: [50, 50],
                            handleSize: 3,
                            onSelect : function() {
                                var selection = jcrop_api.tellSelect(),
                                    scaleX, scaleY, scale;
                                
                                if (!selection.w || !selection.h)
                                    return false;

                                if (selection.w > selection.h) {
                                    
                                } else {
                                    
                                }
                            }
                        }, 
                        function(){
                            jcrop_api = this;
                            // set selection
                            var crop = data.crop ? data.crop : [0.2,0.2,0.3,0.3],
                                w = $cropimage.width(), 
                                h = $cropimage.height();
                            if (crop) {
                                jcrop_api.setSelect([   
                                    crop[0]*w, 
                                    crop[2]*h, 
                                    w - (crop[1]*w),
                                    h - (crop[3]*h)
                                ]);
                            } else {
                                jcrop_api.setSelect([ 0, 0, $cropimage.width(), $cropimage.height() ]);
                            }
                        }
                    );
                },
                _saveCropped : function($slide) {
                    var _this = this,
                        API = this.getAPI(),
                        data = API.getExtensionData($slide, _this),
                        args={}, params=[], crop = [], size = [],
                        l, t, r, b, w, h,
                        selection = jcrop_api.tellSelect(),
                        bounds = jcrop_api.getBounds();
                    
                    for(var key in selection){
                        params.push(selection[key]);
                    }
                    for(var key in bounds){
                        params.push(bounds[key]);
                    }

                    w = params[6];
                    h = params[7];
                    l = params[0]/w;
                    t = params[1]/h;
                    r = (w - params[2])/w;
                    b = (h - params[3])/h;
                    
                    crop = [l,r,t,b];
                    for (var i = 0; i < crop.length; i++) {
                        crop[i] = parseFloat(crop[i]).toFixed(3);
                    }
                    size = [API.get('width', 500,'int'),API.get('height', 350, 'int')];
                    
                    API.updateSlide($slide, { crop: crop, size: size });
                    
                    jcrop_api.destroy();
                },
                /*  public method called from slideshow 
                *   'this' refers to API methods.
                *   required for a slide plugin
                */
                update : function($slide, data) {
                    var API = this.getAPI(),
                        src, desc='';

                    src = API.getThumbSrc({
                            file: data.filename, 
                            folder: data.folder, 
                            size: [API.get('width', 600, 'int'), API.get('height', 400, 'int')], 
                            filters: {
                                crop : data.crop
                            }
                        });
                    width = API.get('width', 600, 'int');
                    
                    desc = data.description ? data.description : {fontSize: '100%', background: 'default', color: '', text: '', position: 'bottom'};

                    $slide.find('img')
                            .css({
                                width: width,
                                height: 'auto'
                            })
                            .attr('src', src)
                        .end()
                        .find('.description')
                            .text(desc.text)
                            .css({'color': desc.color, 'font-size': desc.fontSize })
                        .end().find('.description-wrapper')
                            .css({'top': desc.position == 'top' ? 0 : 'initial', 'bottom': desc.position == 'bottom' ? 0 : 'initial'})
                        .end().find('.background')
                            .css({'background-color': desc.background == 'default' ? '' : desc.background});                     
                }

        } // end of var ImagePlugin
        
        // add plugin
        $("#jbmslideshow").jbmslideshow('addExtension', 'image', ImagePlugin);
    });
} (this.jQuery, this, this.document));    