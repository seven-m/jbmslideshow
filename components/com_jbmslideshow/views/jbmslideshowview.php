<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
jimport('joomla.registry.registry');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');
jimport( 'joomla.utilities.utility' );
/**
 * @package		Joomla.Site
 * @subpackage	com_jbmslideshow
 */
class JbmslideshowView extends JView
{    
    /*
    * displays single slide 
    */
    public function displaySlide($item)
    {
        $helper     = &$this->helper;
        $extensions = $helper->getExtensions();
        
        if (isset($item['extension']) && $item['extension']!='') {
            // check if the extension is published
            if (!isset($extensions[$item['extension']])) return;
      
            $item_reg = new JRegistry();
            $item_reg->loadArray(is_array($item) ? $item : array());
            echo '<div class="slide '.$item['extension'].' " data="'.htmlentities(json_encode($item_reg->toArray())).'">';
            
            $tpl = $item['extension'];
            $file = $this->_layout;
            // clean the file name
            $file = preg_replace('/[^A-Z0-9_\.-]/i', '', $file);
            $tpl  = preg_replace('/[^A-Z0-9_\.-]/i', '', $tpl);
            jimport('joomla.filesystem.path');            
            $filetofind = $this->_createFileName('template', array('name' => $file.'_'.$tpl));
            $this->_template = JPath::find($this->_path['template'], $filetofind);
            
            $this->assignRef($item['extension'], $item_reg);
            
            if ($this->_template && !$this->logged) {
                echo parent::loadTemplate($tpl);
            } else {
                $this->_addPath( 'template', COM_JBMSLIDESHOW_EXTENSIONS . DS. $item['extension'] . DS . 'views' );
                echo $this->loadTemplate($item['extension']); 
            } 
            echo '</div>'; 
        }
    }
    /*
    * overrides parent's loadTemplate
    * to include file from relevant plugin
    */
    function loadTemplate( $tpl = null)
    {
        global $mainframe, $option;
 
        // clear prior output
        $this->_output = null;
 
        //create the template file name based on the layout
        $file = isset($tpl) ? $tpl : $this->_layout;
        // clean the file name
        $file = preg_replace('/[^A-Z0-9_\.-]/i', '', $file);
        $tpl  = preg_replace('/[^A-Z0-9_\.-]/i', '', $tpl);
 
        // load the template script
        jimport('joomla.filesystem.path');
        $filetofind     = $this->_createFileName('template', array('name' => $file));
        $this->_template = JPath::find($this->_path['template'], $filetofind);

        if ($this->_template != false)
        {
            // unset so as not to introduce into template scope
            unset($tpl);
            unset($file);

            // never allow a 'this' property
            if (isset($this->this)) {
                unset($this->this);
            }

            // start capturing output into a buffer
            ob_start();
            // include the requested template filename in the local scope
            // (this will execute the view logic).
            include $this->_template;

            // done with the requested template; get the buffer and
            // clear it.
            $this->_output = ob_get_contents();
            ob_end_clean();

            return $this->_output;
        } else {
            return JError::raiseError( 500, 'Layout "' . $file . '" not found' );
        }
    }
    
    public function addCustomScript($script='')
    {
        $doc = & JFactory::getDocument();
        $url = COM_JBMSLIDESHOW_URL.'/views/'.$this->getName().'/js/';
        if (!preg_match('/^(http\:|https\:)/', $script)) {
            $doc->addScript($url.$script);
        } else {
            $doc->addScript($script);
        }
    }
}
